﻿using AoC19.Common.IntCode;
using System.IO;
using System.Linq;

namespace AoC19.Day9
{
    public sealed class Day9Solver
    {
        private const string InputPath = @"_inputs/day9input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private readonly string _input;

        public Day9Solver(string input)
        {
            _input = input;
        }

        public long[] SolvePart1()
        {
            IntCodeVM vm = new IntCodeVM(_input);
            vm.Input.Enqueue(1);
            vm.Run();
            return vm.Output.ToArray();
        }

        public long[] SolvePart2()
        {
            IntCodeVM vm = new IntCodeVM(_input);
            vm.Input.Enqueue(2);
            vm.Run();
            return vm.Output.ToArray();
        }
    }
}