﻿using AoC19.Common.IntCode;
using AoC19.Day5;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day7
{
    public sealed class Day7Solver
    {
        private const string InputPath = @"_inputs/day7input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private readonly string _input;

        public Day7Solver(string input)
        {
            _input = input;
        }

        public (long output, int[] phaseSetting) SolvePart1()
        {
            long maxOutput = int.MinValue;
            int[] maxPhaseSettings = null;

            for (int i = 01234; i <= 43210; i++)
            {
                int[] phaseSettings = i.ToString().PadLeft(5, '0').Select(ch => (int)char.GetNumericValue(ch)).ToArray();
                if (phaseSettings.Any(p => p > 4))
                    continue;
                if (phaseSettings.Distinct().Count() < 5)
                    continue;

                long output = GetOutputForPhaseSettings(phaseSettings);
                if (output > maxOutput)
                {
                    maxOutput = output;
                    maxPhaseSettings = phaseSettings;
                }
            }

            return (maxOutput, maxPhaseSettings);
        }

        private long GetOutputForPhaseSettings(int[] phaseSettings)
        {
            long currentInput = 0;

            foreach (int phaseSetting in phaseSettings)
            {
                currentInput = RunIteration(phaseSetting, currentInput);
            }

            return currentInput;
        }

        private long RunIteration(int phaseSetting, long amplifierInput)
        {
            IntCodeVM vm = new IntCodeVM(_input);
            vm.Input.Enqueue(phaseSetting);
            vm.Input.Enqueue(amplifierInput);

            vm.Run();

            return vm.Output.Single();
        }

        public (long output, int[] phaseSetting) SolvePart2()
        {
            long maxOutput = int.MinValue;
            int[] maxPhaseSettings = null;

            for (int i = 56789; i <= 98765; i++)
            {
                int[] phaseSettings = i.ToString().PadLeft(5, '0').Select(ch => (int)char.GetNumericValue(ch)).ToArray();
                if (phaseSettings.Any(p => p < 5) || phaseSettings.Distinct().Count() < 5)
                    continue;

                long output = GetOutputForPhaseSettings_Part2(phaseSettings);
                if (output > maxOutput)
                {
                    maxOutput = output;
                    maxPhaseSettings = phaseSettings;
                }
            }

            return (maxOutput, maxPhaseSettings);
        }

        private long GetOutputForPhaseSettings_Part2(int[] phaseSettings)
        {
            IntCodeVM[] amps = PrepareAmpsForPart2(phaseSettings);

            Queue<IntCodeVM> ampQueue = new Queue<IntCodeVM>(amps);

            Queue<long> currentOutput = new Queue<long>();

            while (ampQueue.Any())
            {
                IntCodeVM amp = ampQueue.Dequeue();
                TransferItems(currentOutput, amp.Input);
                HaltType haltType = amp.Run();
                TransferItems(amp.Output, currentOutput);

                if (haltType == HaltType.WaitingForInput)
                    ampQueue.Enqueue(amp);
            }

            return currentOutput.Single();
        }

        private static void TransferItems<T>(Queue<T> from, Queue<T> to)
        {
            while (from.Any())
                to.Enqueue(from.Dequeue());
        }

        private IntCodeVM[] PrepareAmpsForPart2(int[] phaseSettings)
        {
            IntCodeVM[] amps = new IntCodeVM[phaseSettings.Length];
            for (int i = 0; i < amps.Length; i++)
            {
                amps[i] = new IntCodeVM(_input)
                {
                    Name = $"VM{i + 1}"
                };
                amps[i].Input.Enqueue(phaseSettings[i]);
            }

            amps[0].Input.Enqueue(0);

            return amps;
        }
    }
}