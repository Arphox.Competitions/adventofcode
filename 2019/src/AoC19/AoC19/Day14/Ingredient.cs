﻿namespace AoC19.Day14
{
    public sealed class Ingredient
    {
        public int Amount { get; }
        public string Material { get; }

        public Ingredient(int amount, string material)
        {
            Amount = amount;
            Material = material;
        }

        public Ingredient((int amount, string material) input)
            : this(input.amount, input.material)
        { }

        public override string ToString() => $"{Amount} {Material}";
    }
}