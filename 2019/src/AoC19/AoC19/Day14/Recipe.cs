﻿using System.Collections.Generic;
using System.Linq;

namespace AoC19.Day14
{
    public sealed class Recipe
    {
        public int Amount { get; }
        public string Material { get; }
        public IReadOnlyList<Ingredient> Ingredients { get; }

        public Recipe(int amount, string material, IReadOnlyList<Ingredient> ingredients)
        {
            Amount = amount;
            Material = material;
            Ingredients = ingredients;
        }

        public override string ToString()
        {
            string ingredientsPart = string.Join(", ", Ingredients.Select(x => x.ToString()));
            string resultPart = $"{Amount} {Material}";
            return $"{ingredientsPart} => {resultPart}";
        }
    }
}