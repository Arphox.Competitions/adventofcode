﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC19.Day14
{
    public static class Day14InputParser
    {
        public static IReadOnlyList<Recipe> ParseInput(string[] input)
        {
            return input
                .Select(ParseRecipe)
                .ToArray();
        }

        public static Recipe ParseRecipe(string line)
        {
            string[] lineParts = line.Split("=>");
            if (lineParts.Length != 2) throw new ArgumentException($"Invalid input line: '{line}'");

            IReadOnlyList<Ingredient> ingredients = ParseIngredientsPart(lineParts[0]);
            (int amount, string material) = ParseAmountAndMaterial(lineParts[1]);

            return new Recipe(amount, material, ingredients);
        }

        private static IReadOnlyList<Ingredient> ParseIngredientsPart(string ingredientsPart)
        {
            ingredientsPart = ingredientsPart.Trim();
            string[] parts = ingredientsPart.Split(", ");

            return parts
                .Select(p => new Ingredient(ParseAmountAndMaterial(p)))
                .ToArray();
        }

        private static (int amount, string material) ParseAmountAndMaterial(string linePart)
        {
            string[] parts = linePart.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
                throw new ArgumentException($"Invalid result part: '{linePart}'");

            int amount = int.Parse(parts[0]);
            return (amount, parts[1]);
        }
    }
}