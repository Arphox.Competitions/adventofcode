﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day14
{
    public sealed class Day14Solver
    {
        private const string InputPath = @"_inputs/day14input.txt";
        public static string[] ReadRealInput() => File.ReadAllLines(InputPath);

        private readonly IReadOnlyList<string> _input;

        public Day14Solver(string[] input) => _input = input;
        public long SolvePart1(int amount = 1) => new Day14Part1Solver(_input.ToArray()).Solve(amount);
        public long SolvePart2() => new Day14Part2Solver(_input.ToArray()).Solve();
    }
}