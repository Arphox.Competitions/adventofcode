﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC19.Day14
{
    public sealed class Day14Part1Solver
    {
        private readonly Dictionary<string, Recipe> _recipes;
        private readonly StockManager _stockManager;
        private long _usedOres = 0;

        public Day14Part1Solver(string[] input)
        {
            var recipesList = Day14InputParser.ParseInput(input);
            _recipes = recipesList.ToDictionary(key => key.Material);
            _stockManager = new StockManager();
        }

        public long Solve(int amount = 1)
        {
            Craft(amount, "FUEL");
            return _usedOres;
        }

        private void Craft(long requiredAmount, string material)
        {
            requiredAmount = _stockManager.DecreaseWithStockValue(material, requiredAmount);
            if (requiredAmount == 0)
                return;

            if (material == "ORE")
            {
                _usedOres += requiredAmount;
                return;
            }

            Recipe recipe = _recipes[material];
            long recipeRepeatCount = (long)Math.Ceiling(requiredAmount / (double)recipe.Amount);
            recipeRepeatCount = Math.Max(1, recipeRepeatCount);

            foreach (Ingredient ingredient in recipe.Ingredients)
            {
                Craft(ingredient.Amount * recipeRepeatCount, ingredient.Material);
            }

            long waste = recipe.Amount * recipeRepeatCount - requiredAmount;
            _stockManager.Put(material, waste);
        }
    }
}