﻿using System;
using System.IO;
using System.Linq;

namespace AoC19.Day1
{
    class Day1solver
    {
        private const string InputPath = @"_inputs/day1input.txt";

        public void Part1()
        {
            int answer = File.ReadAllLines(InputPath)
                .Select(int.Parse)
                .Select(x => x / 3 - 2)
                .Sum();

            Console.WriteLine(answer);
        }

        public void Part2()
        {
            int answer = File.ReadAllLines(InputPath)
                .Select(int.Parse)
                .Select(CalculatePart2Fuel)
                .Sum();

            Console.WriteLine(answer);
        }

        private static int CalculatePart2Fuel(int x)
        {
            int sum = 0;
            int current = x / 3 - 2;

            while(current > 0)
            {
                sum += current;
                current = current / 3 - 2;
            }

            return sum;
        }
    }
}