﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day10
{
    public sealed class Day10Solver
    {
        private const string InputPath = @"_inputs/day10input.txt";
        public static string[] ReadRealInput() => File.ReadAllLines(InputPath);
        public char[,] Map { get; } // X, Y

        private (int x, int y)[] _allAsteroids;

        public Day10Solver(string[] input)
        {
            int width = input[0].Length;
            int height = input.Length;
            Map = new char[width, height];

            for (int y = 0; y < input.Length; y++)
            {
                string row = input[y];
                for (int x = 0; x < row.Length; x++)
                    Map[x, y] = row[x];
            }

            _allAsteroids = GetAllAsteroids();
        }

        public (int x, int y, int seenAsteroids) SolvePart1()
        {
            int maxIndex = 0;
            int maxValue = -1;
            for (int i = 0; i < _allAsteroids.Length; i++)
            {
                (int x, int y) asteroidPoint = _allAsteroids[i];
                int value = DoDetect(asteroidPoint).Count;

                if (value > maxValue)
                {
                    maxIndex = i;
                    maxValue = value;
                }
            }

            (int x, int y) bestAsteroid = _allAsteroids[maxIndex];
            return (bestAsteroid.x, bestAsteroid.y, maxValue);
        }

        private (int x, int y)[] GetAllAsteroids()
        {
            List<(int x, int y)> allAsteroids = new List<(int x, int y)>();

            for (int x = 0; x < Map.GetLength(0); x++)
                for (int y = 0; y < Map.GetLength(1); y++)
                    if (Map[x, y] == '#')
                        allAsteroids.Add((x, y));

            return allAsteroids.ToArray();
        }

        public List<(int x, int y)> DoDetect((int x, int y) centerPoint)
        {
            List<(int x, int y)> asteroidsForTrial = _allAsteroids.Except(new[] { centerPoint }).ToList();

            int changedCount = asteroidsForTrial.Count;
            int i = 1;
            while (changedCount > 0)
            {
                Console.WriteLine($"Round {i}:");
                changedCount = DoAsteroidCleaningRound(asteroidsForTrial, centerPoint);
                i++;
            }

            return asteroidsForTrial;
        }

        private int DoAsteroidCleaningRound(List<(int x, int y)> asteroidsForTrial, (int x, int y) centerPoint)
        {
            int startCount = asteroidsForTrial.Count;

            for (int i = asteroidsForTrial.Count - 1; i >= 0; i--)
            {
                if (i >= asteroidsForTrial.Count) // if changed meanwhile
                    continue;

                (int x, int y) targetPoint = asteroidsForTrial[i];
                (int x, int y) directionVector = FindDirectionVector(centerPoint, targetPoint);
                if (directionVector.x == 0 && directionVector.y == 0)
                    continue;

                Raycast(asteroidsForTrial, targetPoint, directionVector);
            }

            int finalCount = asteroidsForTrial.Count;

            return startCount - finalCount;
        }

        private void Raycast(List<(int x, int y)> asteroidsForTrial, (int x, int y) asteroidPoint, (int x, int y) directionVector)
        {
            int mapWidth = Map.GetLength(0);
            int mapHeight = Map.GetLength(1);

            int currentX = asteroidPoint.x + directionVector.x;
            int currentY = asteroidPoint.y + directionVector.y;

            while (currentX < mapWidth && currentX >= 0
                && currentY < mapHeight && currentY >= 0)
            {
                // check
                bool isRemoved = asteroidsForTrial.Remove((currentX, currentY)); // WARNING: SLOW! LINEAR SEARCH!
                if (isRemoved)
                    Console.WriteLine($"{asteroidPoint} kicks {(currentX, currentY)}");

                // step
                currentX += directionVector.x;
                currentY += directionVector.y;
            }
        }

        private static (int x, int y) FindDirectionVector((int x, int y) centerPoint, (int x, int y) currentTarget)
        {
            int xDiff = currentTarget.x - centerPoint.x;
            int yDiff = currentTarget.y - centerPoint.y;

            int gcd = FindGreatestCommonDivisor(xDiff, yDiff);

            if (gcd > 0)
            {
                xDiff /= gcd;
                yDiff /= gcd;
            }

            return (xDiff, yDiff);
        }

        public int SolvePart2()
        {
            PointWithAngle[] pointsWithAngles = Part2_GetPointWithAngles();
            PointWithAngle twoHundredth = pointsWithAngles.Skip(199).Take(1).Single();

            return twoHundredth.X * 100 + twoHundredth.Y;
        }

        public PointWithAngle[] Part2_GetPointWithAngles()
        {
            // Here we suppose that only one iteration is enough! (for 200)
            (int stationX, int stationY, int numOfDetectedAsteroids) = SolvePart1();
            List<(int x, int y)> detectedAsteroids = DoDetect((stationX, stationY));
            if (detectedAsteroids.Count != numOfDetectedAsteroids) // sanity check
                throw new Exception("Problem!");

            PointWithAngle[] asteroidsWithAngles = detectedAsteroids
                .Select(p =>
                {
                    int asteroidX = p.x;
                    int asteroidY = p.y;
                    (int directionX, int directionY) = FindDirectionVector((stationX, stationY), (asteroidX, asteroidY));
                    double angle = Day10MathHelper.GetAngle(directionX, directionY);

                    return new PointWithAngle(asteroidX, asteroidY, angle);
                })
                .OrderBy(anon => anon.Angle)
                .ToArray();

            return asteroidsWithAngles;
        }

        private static int FindGreatestCommonDivisor(int a, int b)
        {
            if (a < 0) a = -a; // a = Math.Abs(a)
            if (b < 0) b = -b; // b = Math.Abs(b)

            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            return a == 0 ? b : a;
        }

        public static void PrintMap(char[,] map)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                for (int x = 0; x < map.GetLength(0); x++)
                {
                    Console.Write(map[x, y]);
                }
                Console.WriteLine();
            }
        }
    }
}