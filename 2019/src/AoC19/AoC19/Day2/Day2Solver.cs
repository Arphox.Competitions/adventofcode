﻿using System;
using System.IO;
using System.Linq;

namespace AoC19.Day2
{
    class Day2Solver
    {
        private const string InputPath = @"_inputs/day2input.txt";
        private const int Part2TargetValue = 19690720;

        public void Part1()
        {
            string fileContent = File.ReadAllLines(InputPath)[0];
            int[] memory = fileContent.Split(',', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            RunPart1Program(memory);
            Console.WriteLine(memory[0]);
        }

        private static void RunPart1Program(int[] memory)
        {
            int address = 0;

            while (true)
            {
                int opCode = memory[address];
                if (opCode == 99)
                    return;

                int input1index = memory[address + 1];
                int input2index = memory[address + 2];

                int input1value = memory[input1index];
                int input2value = memory[input2index];

                int outputIndex = memory[address + 3];

                RunPart1ProgramSlice(memory, opCode, input1value, input2value, outputIndex);

                address += 4;
            }
        }

        private static void RunPart1ProgramSlice(int[] memory, int opcode, int input1value, int input2value, int outputIndex)
        {
            switch (opcode)
            {
                case 1: memory[outputIndex] = input1value + input2value; break;
                case 2: memory[outputIndex] = input1value * input2value; break;
            }
        }

        public void Part2()
        {
            string fileContent = File.ReadAllLines(InputPath)[0];
            int[] initialMemory = fileContent.Split(',', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            for (int noun = 0; noun <= 99; noun++)
            {
                for (int verb = 0; verb <= 99; verb++)
                {
                    int[] currentMemory = initialMemory.ToArray();
                    currentMemory[1] = noun;
                    currentMemory[2] = verb;

                    RunPart1Program(currentMemory);
                    int currentValue = currentMemory[0];
                    if (currentValue == Part2TargetValue)
                    {
                        Console.WriteLine(100 * noun + verb);
                        break;
                    }
                }
            }
        }
    }
}