﻿namespace AoC19.Day11
{
    public enum HeadDirection
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3
    }
}