﻿using AoC19.Common.IntCode;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AoC19.Day11
{
    public sealed class Day11Solver
    {
        private const string InputPath = @"_inputs/day11input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private readonly string _input;

        public Day11Solver(string input)
        {
            _input = input;
        }

        public int SolvePart1()
        {
            (int x, int y) currentPos = (0, 0);
            HeadDirection currentDirection = HeadDirection.Up;
            HashSet<(int x, int y)> paintedPositions = new HashSet<(int x, int y)>();
            Dictionary<(int x, int y), PaintColor> positionColors = new Dictionary<(int x, int y), PaintColor>();

            IntCodeVM vm = new IntCodeVM(_input);

            while (true)
            {
                PaintColor paintColorUnderRobot = GetPaintColorUnderRobot(positionColors, currentPos);
                vm.Input.Enqueue((long)paintColorUnderRobot);
                HaltType haltType = vm.Run();
                if (haltType == HaltType.Terminated)
                    return paintedPositions.Count;

                long paintColorOutput = vm.Output.Dequeue();
                long turnDirectionOutput = vm.Output.Dequeue();

                PaintColor paintColor = GetPaintColor(paintColorOutput);

                // Paint
                positionColors[currentPos] = paintColor;
                paintedPositions.Add(currentPos);

                // Turn
                currentDirection = Turn(turnDirectionOutput, currentDirection);

                // Move
                currentPos = Move1Step(currentPos.x, currentPos.y, currentDirection);
            }

            throw new Exception("This should not happen");
        }

        private PaintColor GetPaintColorUnderRobot(Dictionary<(int x, int y), PaintColor> positionColors, (int x, int y) currentPos)
        {
            if (positionColors.TryGetValue(currentPos, out PaintColor color))
                return color;
            else
                return PaintColor.Black;
        }

        private (int x, int y) Move1Step(int currentX, int currentY, HeadDirection currentDirection)
        {
            return currentDirection switch
            {
                HeadDirection.Right => (currentX + 1, currentY),
                HeadDirection.Left => (currentX - 1, currentY),
                HeadDirection.Up => (currentX, currentY - 1),
                HeadDirection.Down => (currentX, currentY + 1),
            };
        }

        private static PaintColor GetPaintColor(long paintColorOutput)
        {
            int value = (int)paintColorOutput;

            if (!Enum.IsDefined(typeof(PaintColor), value))
            {
                Debugger.Break();
                throw new Exception("Invalid output from the IntCodeVM");
            }

            return (PaintColor)value;
        }
        private static HeadDirection Turn(long turnDirection, HeadDirection currentDirection)
        {
            return turnDirection switch
            {
                0 => TurnLeft(),
                1 => TurnRight(),
            };

            // ---------------------
            HeadDirection TurnLeft()
            {
                return currentDirection switch
                {
                    HeadDirection.Up => HeadDirection.Left,
                    HeadDirection.Left => HeadDirection.Down,
                    HeadDirection.Down => HeadDirection.Right,
                    HeadDirection.Right => HeadDirection.Up,
                };
            }

            HeadDirection TurnRight()
            {
                return currentDirection switch
                {
                    HeadDirection.Up => HeadDirection.Right,
                    HeadDirection.Right => HeadDirection.Down,
                    HeadDirection.Down => HeadDirection.Left,
                    HeadDirection.Left => HeadDirection.Up,
                };
            }
        }

        public Dictionary<(int x, int y), PaintColor> SolvePart2()
        {
            (int x, int y) currentPos = (0, 0);
            HeadDirection currentDirection = HeadDirection.Up;
            HashSet<(int x, int y)> paintedPositions = new HashSet<(int x, int y)>();
            Dictionary<(int x, int y), PaintColor> positionColors = new Dictionary<(int x, int y), PaintColor>();

            IntCodeVM vm = new IntCodeVM(_input);
            bool isFirstStep = true;

            while (true)
            {
                long input = GetInput(currentPos, positionColors, ref isFirstStep);
                vm.Input.Enqueue(input);

                HaltType haltType = vm.Run();
                if (haltType == HaltType.Terminated)
                    return positionColors;

                long paintColorOutput = vm.Output.Dequeue();
                long turnDirectionOutput = vm.Output.Dequeue();

                PaintColor paintColor = GetPaintColor(paintColorOutput);

                // Paint
                positionColors[currentPos] = paintColor;
                paintedPositions.Add(currentPos);

                // Turn
                currentDirection = Turn(turnDirectionOutput, currentDirection);

                // Move
                currentPos = Move1Step(currentPos.x, currentPos.y, currentDirection);
            }

            throw new Exception("This should not happen");
        }

        private long GetInput((int x, int y) currentPos, Dictionary<(int x, int y), PaintColor> positionColors, ref bool isFirstStep)
        {
            long input;

            if (isFirstStep)
            {
                input = (long)PaintColor.White;
                isFirstStep = false;
            }
            else
            {
                PaintColor paintColorUnderRobot = GetPaintColorUnderRobot(positionColors, currentPos);
                input = (long)paintColorUnderRobot;
            }

            return input;
        }
    }
}