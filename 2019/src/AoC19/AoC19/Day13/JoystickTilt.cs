﻿namespace AoC19.Day13
{
    public enum JoystickTilt
    {
        Neutral = 0,
        Left = -1,
        Right = 1
    }
}