﻿using AoC19.Common.IntCode;
using System.IO;
using System.Linq;

namespace AoC19.Day5
{
    public sealed class Day5Solver
    {
        public const int AirConditionerUnitId = 1;
        public const int ThermalRadiatorControllerId = 5;
        private const string InputPath = @"_inputs/day5input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private IntCodeVM _intCodeVM;

        public Day5Solver(string input)
        {
            _intCodeVM = new IntCodeVM(input);
        }

        public long Solve(int systemId)
        {
            _intCodeVM.Reset();
            _intCodeVM.Input.Enqueue(systemId);

            _intCodeVM.Run();

            return _intCodeVM.Output.Last();
        }
    }
}