﻿using AoC19.Day16;

namespace AoC19
{
    class Program
    {
        static void Main()
        {
            var solver = new Day16Solver(Day16Solver.ReadRealInput());
            string result = solver.SolvePart1();
        }
    }
}