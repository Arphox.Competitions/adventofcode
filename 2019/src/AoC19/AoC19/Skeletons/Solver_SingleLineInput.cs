﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day98
{
    public sealed class Day98Solver
    {
        private const string InputPath = @"_inputs/day98input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private readonly string _input;

        public Day98Solver(string input)
        {
            _input = input;
        }

        public long SolvePart1()
        {
            return 0;
        }

        public long SolvePart2()
        {
            return 0;
        }
    }
}