﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day99
{
    public sealed class Day99Solver
    {
        private const string InputPath = @"_inputs/day99input.txt";
        public static string[] ReadRealInput() => File.ReadAllLines(InputPath);

        private readonly string[] _input;

        public Day99Solver(string[] input)
        {
            _input = input;
        }

        public long SolvePart1()
        {
            return 0;
        }

        public long SolvePart2()
        {
            return 0;
        }
    }
}