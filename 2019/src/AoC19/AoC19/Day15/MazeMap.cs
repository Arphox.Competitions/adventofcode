﻿using System.Collections.Generic;

namespace AoC19.Day15
{
    public sealed class MazeMap
    {
        public IReadOnlyDictionary<(int x, int y), MazeObjectType> MapView => _map;
        public IReadOnlyCollection<(int x, int y)> Paths => _pathsCache;

        private readonly Dictionary<(int x, int y), MazeObjectType> _map = new Dictionary<(int x, int y), MazeObjectType>();
        private readonly HashSet<(int x, int y)> _pathsCache = new HashSet<(int x, int y)>();

        public MazeObjectType this[(int x, int y) pos]
        {
            get
            {
                _map.TryGetValue(pos, out MazeObjectType value);
                return value;
            }
            set
            {
                if (value == MazeObjectType.Path)
                    _pathsCache.Add(pos);

                _map[pos] = value;
            }
        }
    }
}