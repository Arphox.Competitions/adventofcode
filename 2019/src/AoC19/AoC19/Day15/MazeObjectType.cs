﻿namespace AoC19.Day15
{
    public enum MazeObjectType
    {
        Unknown = 0,
        Path = 1,
        Wall = 2,
        Target = 3
    }
}