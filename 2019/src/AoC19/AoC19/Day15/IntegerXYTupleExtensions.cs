﻿namespace AoC19.Day15
{
    public static class IntegerXYTupleExtensions
    {
        public static (int x, int y) PositionToRight(this (int x, int y) pos) => (pos.x + 1, pos.y);
        public static (int x, int y) PositionToLeft(this (int x, int y) pos) => (pos.x - 1, pos.y);
        public static (int x, int y) PositionToUp(this (int x, int y) pos) => (pos.x, pos.y - 1);
        public static (int x, int y) PositionToDown(this (int x, int y) pos) => (pos.x, pos.y + 1);
    }
}
