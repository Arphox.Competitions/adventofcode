﻿using AoC19.Common.IntCode;
using GridPathFinder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.PortableExecutable;

namespace AoC19.Day15
{
    public sealed class Day15Solver
    {
        private const string InputPath = @"_inputs/day15input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private readonly IntCodeVM _vm;
        private readonly bool _shouldPrint;
        private readonly MazeMap _map;
        private (int x, int y) _currentPos;
        private (int x, int y) _startPos;
        private (int x, int y)? _targetPos;
        private Direction _currentDirection = Direction.Left;
        private Direction? _nextCommandOverride = null;
        private bool _didWeMove = false;

        public Day15Solver(string input, bool shouldPrint = true)
        {
            _shouldPrint = shouldPrint;
            _map = new MazeMap();
            _vm = new IntCodeVM(input);
        }

        public int SolvePart1()
        {
            PrepareRun();

            while (IsThereAnyUnknownMapPart())
                DoStep();

            // Map is discovered here
            Tile[] tiles = _map.MapView.Select(x =>
            {
                (int x, int y) coords = x.Key;
                TileType tileType = coords == _startPos ? TileType.Self : MazeObjectTypeToTileType(x.Value);
                return new Tile(coords.y, coords.x, tileType);
            }).ToArray();

            TileDistanceCalculator waveFrontCalculator = new TileDistanceCalculator(tiles);
            Dictionary<Tile, int> tileDistances = waveFrontCalculator.CalculateTileDistances();

            (int x, int y) targetPosition = _targetPos.Value;
            int distanceToTarget = tileDistances[new Tile(targetPosition.y, targetPosition.x, TileType.Target)];

            return distanceToTarget;
        }

        public int SolvePart2()
        {
            PrepareRun();

            while (IsThereAnyUnknownMapPart())
                DoStep();

            // Map is discovered here
            Tile[] tiles = _map.MapView.Select(x =>
            {
                (int x, int y) coords = x.Key;

                TileType tileType;
                if (coords == _targetPos)
                    tileType = TileType.Self;
                else if (coords == _startPos)
                    tileType = TileType.Target;
                else
                    tileType = MazeObjectTypeToTileType(x.Value);

                return new Tile(coords.y, coords.x, tileType);
            }).ToArray();

            TileDistanceCalculator waveFrontCalculator = new TileDistanceCalculator(tiles);
            Dictionary<Tile, int> tileDistances = waveFrontCalculator.CalculateTileDistances();

            int largestDistance = tileDistances.Values.Max();

            return largestDistance;
        }

        private static TileType MazeObjectTypeToTileType(MazeObjectType mazeObjectType)
        {
            return mazeObjectType switch
            {
                MazeObjectType.Wall => TileType.Wall,
                MazeObjectType.Target => TileType.Target,
                MazeObjectType.Path => TileType.Empty,
                MazeObjectType.Unknown => TileType.Wall,
            };
        }

        private void DoStep()
        {
            Direction command = GetCommand();
            command.ThrowIfNotValidEnumMember();
            StatusCode statusCode = DoIntcodeVmStep(command);
            (int x, int y) newPosition = CalculateNewPosition(command);
            UpdateMap(statusCode, newPosition);
            MoveAndPrint(statusCode, newPosition);
        }

        private void MoveAndPrint(StatusCode statusCode, (int x, int y) newPosition)
        {
            if (statusCode != StatusCode.HitWallDidNotMove)
            {
                Print(_currentPos.x, _currentPos.y, '.');
                _currentPos = newPosition;
                char ch = statusCode == StatusCode.Moved ? 'D' : 'X';
                Print(_currentPos.x, _currentPos.y, ch);
                _didWeMove = true;
            }
            else
            {
                Print(newPosition.x, newPosition.y, '#');
                _didWeMove = false;
            }
        }

        private StatusCode DoIntcodeVmStep(Direction command)
        {
            _vm.Input.Enqueue((int)command);
            _vm.Run();
            StatusCode statusCode = (StatusCode)_vm.Output.Dequeue();

            if (_vm.Output.Count > 0)
                throw new Exception();

            return statusCode;
        }

        private Direction GetCommand()
        {
            Direction? revealingCommand = GetUnknownRevealingCommand();
            if (revealingCommand != null)
                return revealingCommand.Value;
            else
                return GetDiscoveryCommand();
        }

        private Direction? GetUnknownRevealingCommand()
        {
            // Step back to original position if needed
            if (_nextCommandOverride != null)
            {
                Direction temp = _nextCommandOverride.Value;
                _nextCommandOverride = null;
                if (_didWeMove) // if we did not bump into a wall, we have to step back
                    return temp;
            }

            // Try step to unknown
            MazeObjectType[] neighbors = GetMyNeighborsIndexedByDirection();
            for (int i = 1; i < neighbors.Length; i++)
            {
                if (neighbors[i] == MazeObjectType.Unknown)
                {
                    Direction command = (Direction)i;
                    _nextCommandOverride = command.GetOpposite();
                    return command;
                }
            }

            return null;
        }

        private Direction GetDiscoveryCommand()
        {
            // Can turn right?
            MazeObjectType objectIfTurnRight = _map[CalculateNewPosition(_currentDirection.TurnRight())];
            if (objectIfTurnRight != MazeObjectType.Wall)
            {
                _currentDirection = _currentDirection.TurnRight();
                return _currentDirection;
            }

            // Can go forward?
            MazeObjectType objectInFrontOfMe = _map[CalculateNewPosition(_currentDirection)];
            if (objectInFrontOfMe != MazeObjectType.Wall)
            {
                return _currentDirection;
            }

            // Can turn left?
            MazeObjectType objectIfTurnLeft = _map[CalculateNewPosition(_currentDirection.TurnLeft())];
            if (objectIfTurnLeft != MazeObjectType.Wall)
            {
                _currentDirection = _currentDirection.TurnLeft();
                return _currentDirection;
            }

            // Turn around, go back
            _currentDirection = _currentDirection.GetOpposite();
            return _currentDirection;
        }

        private void PrepareRun()
        {
            const int width = 42;
            const int height = 42;

            if (_shouldPrint)
            {
                Console.CursorVisible = false;
                Console.SetWindowSize(width, height);
            }

            _currentPos = (width / 2, height / 2);
            _startPos = _currentPos;

            Print(_currentPos.x, _currentPos.y, 'D');
            _map[_currentPos] = MazeObjectType.Path;
        }
        private void UpdateMap(StatusCode statusCode, (int x, int y) position)
        {
            switch (statusCode)
            {
                case StatusCode.HitWallDidNotMove: _map[position] = MazeObjectType.Wall; break;
                case StatusCode.Moved: _map[position] = MazeObjectType.Path; break;
                case StatusCode.MovedAndFoundTarget:
                    {
                        _map[position] = MazeObjectType.Target;
                        _targetPos = position;
                        break;
                    }
            }
        }
        private void Print(int x, int y, char c)
        {
            if (_shouldPrint)
            {
                Console.SetCursorPosition(x, y);
                Console.Write(c);
            }
        }
        private bool IsThereAnyUnknownMapPart()
        {
            foreach ((int x, int y) in _map.Paths)
            {
                var neighbor = _map[(x + 1, y)];
                if (neighbor == MazeObjectType.Unknown) return true;

                neighbor = _map[(x - 1, y)];
                if (neighbor == MazeObjectType.Unknown) return true;

                neighbor = _map[(x, y + 1)];
                if (neighbor == MazeObjectType.Unknown) return true;

                neighbor = _map[(x, y - 1)];
                if (neighbor == MazeObjectType.Unknown) return true;
            }

            return false;
        }
        private MazeObjectType[] GetMyNeighborsIndexedByDirection()
        {
            var arr = new MazeObjectType[5];

            arr[0] = MazeObjectType.Wall;
            arr[(int)Direction.Up] = _map[_currentPos.GetNeighborPositionTowards(Direction.Up)];
            arr[(int)Direction.Down] = _map[_currentPos.GetNeighborPositionTowards(Direction.Down)];
            arr[(int)Direction.Left] = _map[_currentPos.GetNeighborPositionTowards(Direction.Left)];
            arr[(int)Direction.Right] = _map[_currentPos.GetNeighborPositionTowards(Direction.Right)];

            return arr;
        }
        private (int x, int y) CalculateNewPosition(Direction command) => _currentPos.GetNeighborPositionTowards(command);
    }
}