﻿using AoC19.Common.IntCode.InstructionHandlers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC19.Common.IntCode
{
    public sealed class IntCodeVM
    {
        private readonly long[] _initialMemory;
        public long[] Memory { get; private set; }
        private long _instructionPointer;
        private long _relativeBaseOffset = 0;
        private Dictionary<OpCode, IInstructionHandler> _instructionHandlerDictionary;

        public Queue<long> Input { get; }
        public Queue<long> Output { get; }
        public string Name { get; set; }

        public IntCodeVM(string initialMemory)
        {
            long[] program = initialMemory.Split(',')
                .Select(long.Parse)
                .ToArray();

            long initialMemorySize = program.Length * 3 + 100;
            _initialMemory = new long[initialMemorySize];
            Array.Copy(program, 0, _initialMemory, 0, program.Length);

            Input = new Queue<long>();
            Output = new Queue<long>();
            _instructionHandlerDictionary = CreateInstructionHandlerDictionary();
            Reset();
        }

        public void Reset()
        {
            Memory = _initialMemory.ToArray();
            _instructionPointer = 0;
            _relativeBaseOffset = 0;
            Input.Clear();
            Output.Clear();
        }

        public HaltType Run()
        {
            while (true)
            {
                long instruction = Memory[_instructionPointer++];
                (OpCode opCode, ParameterMode[] paramModes) = ParseInstruction(instruction);

                if (opCode == OpCode.Halt)
                    return HaltType.Terminated;
                if (opCode == OpCode.Read && Input.Count == 0)
                {
                    _instructionPointer--;
                    return HaltType.WaitingForInput;
                }

                _instructionHandlerDictionary[opCode].Handle(Memory, ref _instructionPointer, paramModes);
            }
        }

        private static (OpCode, ParameterMode[]) ParseInstruction(long instruction)
        {
            OpCode opCode = (OpCode)(instruction % 100);
            ParameterMode param1Mode = (ParameterMode)(instruction % 1000 / 100);
            ParameterMode param2Mode = (ParameterMode)(instruction % 10000 / 1000);
            ParameterMode param3Mode = (ParameterMode)(instruction % 100000 / 10000);

            ParameterMode[] parameterModes = new[] { param1Mode, param2Mode, param3Mode };

            return (opCode, parameterModes);
        }

        private Dictionary<OpCode, IInstructionHandler> CreateInstructionHandlerDictionary()
        {
            Func<long> relativeBaseOffsetProvider = () => _relativeBaseOffset;

            return new Dictionary<OpCode, IInstructionHandler>()
            {
                { OpCode.Add, new AddInstructionHandler(relativeBaseOffsetProvider) },
                { OpCode.Multiply, new MultiplyInstructionHandler(relativeBaseOffsetProvider) },
                { OpCode.Read, new ReadInstructionHandler(Input, relativeBaseOffsetProvider) },
                { OpCode.Print, new PrintInstructionHandler(Output, relativeBaseOffsetProvider) },
                { OpCode.JumpIfTrue, new JumpIfTrueInstructionHandler(relativeBaseOffsetProvider) },
                { OpCode.JumpIfFalse, new JumpIfFalseInstructionHandler(relativeBaseOffsetProvider) },
                { OpCode.LessThan, new LessThanInstructionHandler(relativeBaseOffsetProvider) },
                { OpCode.Equals, new EqualsInstructionHandler(relativeBaseOffsetProvider) },
                { OpCode.IncRelativeBase, new IncRelativeBaseInstructionHandler(x => _relativeBaseOffset +=x , relativeBaseOffsetProvider) }
            };
        }

        public override string ToString() => Name;
    }
}