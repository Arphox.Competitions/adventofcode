﻿using System;
using System.Collections.Generic;

namespace AoC19.Common.IntCode.InstructionHandlers
{
    public sealed class PrintInstructionHandler : InstructionHandler
    {
        private readonly Queue<long> _output;

        public PrintInstructionHandler(Queue<long> output, Func<long> relativeBaseOffsetProvider)
            : base(relativeBaseOffsetProvider)
        {
            _output = output;
        }

        public override void Handle(long[] memory, ref long IP, ParameterMode[] paramModes)
        {
            long param = memory[IP++];
            long paramValue = GetParamValue(param, paramModes[0], memory);

            _output.Enqueue(paramValue);
        }
    }
}