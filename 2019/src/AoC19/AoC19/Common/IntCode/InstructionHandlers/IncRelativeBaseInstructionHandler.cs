﻿using System;

namespace AoC19.Common.IntCode.InstructionHandlers
{
    public sealed class IncRelativeBaseInstructionHandler : InstructionHandler
    {
        private readonly Action<long> _relativeBaseIncreaser;

        public IncRelativeBaseInstructionHandler(Action<long> relativeBaseIncreaser, Func<long> relativeBaseOffsetProvider)
            : base(relativeBaseOffsetProvider)
        {
            _relativeBaseIncreaser = relativeBaseIncreaser;
        }

        public override void Handle(long[] memory, ref long IP, ParameterMode[] paramModes)
        {
            long param = memory[IP++];
            long paramValue = GetParamValue(param, paramModes[0], memory);

            _relativeBaseIncreaser(paramValue);
        }
    }
}