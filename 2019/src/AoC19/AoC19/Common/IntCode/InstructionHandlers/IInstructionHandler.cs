﻿namespace AoC19.Common.IntCode.InstructionHandlers
{
    public interface IInstructionHandler
    {
        void Handle(long[] memory, ref long IP, ParameterMode[] paramModes);
    }
}