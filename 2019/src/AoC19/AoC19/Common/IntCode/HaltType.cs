﻿namespace AoC19.Common.IntCode
{
    public enum HaltType
    {
        Terminated = 0,
        WaitingForInput = 1
    }
}