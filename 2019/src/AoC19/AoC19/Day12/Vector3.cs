﻿using System;

namespace AoC19.Day12
{
    public struct Vector3
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public Vector3(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public int CalculateEnergy() => Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z);

        public void AddToThis(Vector3 other)
        {
            X += other.X;
            Y += other.Y;
            Z += other.Z;
        }

        public override string ToString() => $"({X}, {Y}, {Z})";
        
        public static Vector3 Parse(string input)
        {
            input = input.Replace("<", "");
            input = input.Replace(">", "");
            string[] parts = input.Split(", ", StringSplitOptions.RemoveEmptyEntries);

            if (!parts[0].StartsWith("x=")) throw new Exception();
            if (!parts[1].StartsWith("y=")) throw new Exception();
            if (!parts[2].StartsWith("z=")) throw new Exception();

            parts[0] = parts[0].Substring(2, parts[0].Length - 2);
            parts[1] = parts[1].Substring(2, parts[1].Length - 2);
            parts[2] = parts[2].Substring(2, parts[2].Length - 2);

            int x = int.Parse(parts[0]);
            int y = int.Parse(parts[1]);
            int z = int.Parse(parts[2]);

            return new Vector3(x, y, z);
        }
    }
}