﻿using System;
using System.IO;
using System.Linq;

namespace AoC19.Day8
{
    public sealed class Day8Solver
    {
        public const int Part1Width = 25; // X
        public const int Part1Height = 6; // Y
        private const string InputPath = @"_inputs/day8input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();
        private readonly int[][,] _layers;
        private readonly int _width;
        private readonly int _height;

        public Day8Solver(string input, int width, int height)
        {
            bool isCorrect = input.Length % (width * height) == 0;
            if (!isCorrect)
                throw new Exception("Wrong picture?");

            int[] inputDigits = input
                .Select(ch => (int)char.GetNumericValue(ch))
                .ToArray();

            int numberOfLayers = inputDigits.Length / (width * height);

            _layers = new int[numberOfLayers][,]; // LayerIndex -> X,Y

            int digitCounter = 0;
            for (int layerCounter = 0; layerCounter < numberOfLayers; layerCounter++)
            {
                _layers[layerCounter] = new int[width, height];
                int[,] layer = _layers[layerCounter];
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        layer[x, y] = inputDigits[digitCounter++];
                    }
                }
            }

            _width = width;
            _height = height;
        }

        public int SolvePart1()
        {
            int minIndex = 0;
            for (int i = 1; i < _layers.Length; i++)
                if (GetNumberOfDigitsThatIs(_layers[i], 0) < GetNumberOfDigitsThatIs(_layers[minIndex], 0))
                    minIndex = i;

            int[,] chosenLayer = _layers[minIndex];

            int numOf1digits = GetNumberOfDigitsThatIs(chosenLayer, 1);
            int numOf2digits = GetNumberOfDigitsThatIs(chosenLayer, 2);

            int result = numOf1digits * numOf2digits;
            return result;
        }

        private int GetNumberOfDigitsThatIs(int[,] layer, int targetValue)
        {
            return layer.Cast<int>().Count(d => d == targetValue);
        }

        public string SolvePart2()
        {
            int[,] renderedImage = new int[_width, _height];

            for (int x = 0; x < renderedImage.GetLength(0); x++)
                for (int y = 0; y < renderedImage.GetLength(1); y++)
                    renderedImage[x, y] = FindRenderedPixel(x, y);

            // Print rendered image to console
            for (int y = 0; y < renderedImage.GetLength(1); y++)
            {
                for (int x = 0; x < renderedImage.GetLength(0); x++)
                {
                    switch (renderedImage[x, y])
                    {
                        case 0: Console.BackgroundColor = ConsoleColor.Black; break;
                        case 1: Console.BackgroundColor = ConsoleColor.White; break;
                        case 2: Console.BackgroundColor = ConsoleColor.Green; break;
                    }

                    Console.Write(' ');
                }

                Console.WriteLine();
            }

            return "";
        }

        private int FindRenderedPixel(int x, int y)
        {
            for (int i = 0; i < _layers.Length; i++)
            {
                int currentPixel = _layers[i][x, y];
                if (currentPixel != 2)
                    return currentPixel;
            }

            return 2;
        }
    }
}