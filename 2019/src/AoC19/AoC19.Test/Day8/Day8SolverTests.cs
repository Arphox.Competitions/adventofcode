﻿using AoC19.Day8;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day8
{
    public sealed class Day8SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day8SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1WebsiteExample()
        {
            var solver = new Day8Solver("123456789012", 3, 2);
            var result = solver.SolvePart1();
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day8Solver(Day8Solver.ReadRealInput(), Day8Solver.Part1Width, Day8Solver.Part1Height);
            var result = solver.SolvePart1();
            result.Should().Be(2806);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day8Solver(Day8Solver.ReadRealInput(), Day8Solver.Part1Width, Day8Solver.Part1Height);
            var result = solver.SolvePart2();
        }
    }
}