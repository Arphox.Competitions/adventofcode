﻿using AoC19.Day99;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day99
{
    public sealed class Day99SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day99SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day99Solver(Day99Solver.ReadRealInput());
            var result = solver.SolvePart1();
            result.Should().Be(0);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day99Solver(Day99Solver.ReadRealInput());
            var result = solver.SolvePart2();
            result.Should().Be(0);
        }
    }
}