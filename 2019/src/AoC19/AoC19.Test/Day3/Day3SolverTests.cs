using AoC19.Day3;
using FluentAssertions;
using Xunit;

namespace AoC19.Test.Day3
{
    public class Day3SolverTests
    {
        [Theory]
        [InlineData("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 159)]
        [InlineData("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135)]
        public void Part1WebsiteExamples(string wire1path, string wire2path, int expectedDistance)
        {
            var solver = new Day3Solver(new[] { wire1path, wire2path });
            int actualResult = solver.SolvePart1();

            actualResult.Should().Be(expectedDistance);
        }

        [Fact]
        public void Part1RealInput()
        {
            const int expectedResult = 1084;
            var solver = new Day3Solver(Day3Solver.ReadRealInput());
            int result = solver.SolvePart1();
            result.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData("R8,U5,L5,D3", "U7,R6,D4,L4", 30)]
        [InlineData("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 610)]
        [InlineData("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410)]
        public void Part2WebsiteExamples(string wire1path, string wire2path, int expectedDistance)
        {
            var solver = new Day3Solver(new[] { wire1path, wire2path });
            int actualResult = solver.SolvePart2();

            actualResult.Should().Be(expectedDistance);
        }

        [Theory]
        [InlineData("R9,U5,L6,D3", "U7,R6,D4,L4", 32)]
        public void Part2MyExamples(string wire1path, string wire2path, int expectedDistance)
        {
            var solver = new Day3Solver(new[] { wire1path, wire2path });
            int actualResult = solver.SolvePart2();

            actualResult.Should().Be(expectedDistance);
        }

        [Fact]
        public void Part2RealInput()
        {
            const int expectedResult = 9240;
            var solver = new Day3Solver(Day3Solver.ReadRealInput());
            int result = solver.SolvePart2();
            result.Should().Be(expectedResult);
        }
    }
}