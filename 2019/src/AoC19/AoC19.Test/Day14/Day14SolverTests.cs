﻿using AoC19.Day14;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day14
{
    public sealed class Day14SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day14SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1_OnlyOreForFuel()
        {
            const string input = "30 ORE => 1 FUEL";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(30);
        }

        [Fact]
        public void Part1_OneIngredientForFuel_NoWaste()
        {
            const string input =
                "10 A => 1 FUEL" + "\n" +
                "2 ORE => 2 A";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(10);
        }

        [Fact]
        public void Part1WebsiteExample1()
        {
            const string input =
                "10 ORE => 10 A" + "\n" +
                "1 ORE => 1 B" + "\n" +
                "7 A, 1 B => 1 C" + "\n" +
                "7 A, 1 C => 1 D" + "\n" +
                "7 A, 1 D => 1 E" + "\n" +
                "7 A, 1 E => 1 FUEL";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(31);
        }

        [Fact]
        public void Part1WebsiteExample2()
        {
            const string input =
                "9 ORE => 2 A" + "\n" +
                "8 ORE => 3 B" + "\n" +
                "7 ORE => 5 C" + "\n" +
                "3 A, 4 B => 1 AB" + "\n" +
                "5 B, 7 C => 1 BC" + "\n" +
                "4 C, 1 A => 1 CA" + "\n" +
                "2 AB, 3 BC, 4 CA => 1 FUEL";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(165);
        }

        [Fact]
        public void Part1WebsiteExample3()
        {
            const string input =
                "157 ORE => 5 NZVS" + "\n" +
                "165 ORE => 6 DCFZ" + "\n" +
                "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL" + "\n" +
                "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ" + "\n" +
                "179 ORE => 7 PSHF" + "\n" +
                "177 ORE => 5 HKGWZ" + "\n" +
                "7 DCFZ, 7 PSHF => 2 XJWVT" + "\n" +
                "165 ORE => 2 GPVTF" + "\n" +
                "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(13312);
        }

        [Fact]
        public void Part1WebsiteExample4()
        {
            const string input =
                "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG" + "\n" +
                "17 NVRVD, 3 JNWZP => 8 VPVL" + "\n" +
                "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL" + "\n" +
                "22 VJHF, 37 MNCFX => 5 FWMGM" + "\n" +
                "139 ORE => 4 NVRVD" + "\n" +
                "144 ORE => 7 JNWZP" + "\n" +
                "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC" + "\n" +
                "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV" + "\n" +
                "145 ORE => 6 MNCFX" + "\n" +
                "1 NVRVD => 8 CXFTF" + "\n" +
                "1 VJHF, 6 MNCFX => 4 RFSQX" + "\n" +
                "176 ORE => 6 VJHF";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(180697);
        }

        [Fact]
        public void Part1WebsiteExample5()
        {
            const string input =
                "171 ORE => 8 CNZTR" + "\n" +
                "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL" + "\n" +
                "114 ORE => 4 BHXH" + "\n" +
                "14 VRPVC => 6 BMBT" + "\n" +
                "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL" + "\n" +
                "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT" + "\n" +
                "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW" + "\n" +
                "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW" + "\n" +
                "5 BMBT => 4 WPTQ" + "\n" +
                "189 ORE => 9 KTJDG" + "\n" +
                "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP" + "\n" +
                "12 VRPVC, 27 CNZTR => 2 XDBXC" + "\n" +
                "15 KTJDG, 12 BHXH => 5 XCVML" + "\n" +
                "3 BHXH, 2 VRPVC => 7 MZWV" + "\n" +
                "121 ORE => 7 VRPVC" + "\n" +
                "7 XCVML => 6 RJRHP" + "\n" +
                "5 BHXH, 4 VRPVC => 5 LTCX";

            var solver = new Day14Solver(input.Split('\n'));
            long result = solver.SolvePart1();
            result.Should().Be(2210736);
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day14Solver(Day14Solver.ReadRealInput());
            long result = solver.SolvePart1();
            result.Should().Be(873899);
        }

        [Fact] // Should run in milliseconds
        public void Part1RealInput_PerfTest()
        {
            var solver = new Day14Solver(Day14Solver.ReadRealInput());
            solver.SolvePart1(int.MaxValue);
        }

        [Fact]
        public void Part2WebsiteExample3()
        {
            const string input =
                "157 ORE => 5 NZVS" + "\n" +
                "165 ORE => 6 DCFZ" + "\n" +
                "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL" + "\n" +
                "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ" + "\n" +
                "179 ORE => 7 PSHF" + "\n" +
                "177 ORE => 5 HKGWZ" + "\n" +
                "7 DCFZ, 7 PSHF => 2 XJWVT" + "\n" +
                "165 ORE => 2 GPVTF" + "\n" +
                "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";

            var solver = new Day14Solver(input.Split('\n'));
            solver.SolvePart2().Should().Be(82892753L);
        }

        [Fact]
        public void Part2WebsiteExample4()
        {
            const string input =
                "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG" + "\n" +
                "17 NVRVD, 3 JNWZP => 8 VPVL" + "\n" +
                "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL" + "\n" +
                "22 VJHF, 37 MNCFX => 5 FWMGM" + "\n" +
                "139 ORE => 4 NVRVD" + "\n" +
                "144 ORE => 7 JNWZP" + "\n" +
                "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC" + "\n" +
                "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV" + "\n" +
                "145 ORE => 6 MNCFX" + "\n" +
                "1 NVRVD => 8 CXFTF" + "\n" +
                "1 VJHF, 6 MNCFX => 4 RFSQX" + "\n" +
                "176 ORE => 6 VJHF";

            var solver = new Day14Solver(input.Split('\n'));
            solver.SolvePart2().Should().Be(5586022L);
        }

        [Fact]
        public void Part2WebsiteExample5()
        {
            const string input =
                "171 ORE => 8 CNZTR" + "\n" +
                "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL" + "\n" +
                "114 ORE => 4 BHXH" + "\n" +
                "14 VRPVC => 6 BMBT" + "\n" +
                "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL" + "\n" +
                "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT" + "\n" +
                "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW" + "\n" +
                "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW" + "\n" +
                "5 BMBT => 4 WPTQ" + "\n" +
                "189 ORE => 9 KTJDG" + "\n" +
                "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP" + "\n" +
                "12 VRPVC, 27 CNZTR => 2 XDBXC" + "\n" +
                "15 KTJDG, 12 BHXH => 5 XCVML" + "\n" +
                "3 BHXH, 2 VRPVC => 7 MZWV" + "\n" +
                "121 ORE => 7 VRPVC" + "\n" +
                "7 XCVML => 6 RJRHP" + "\n" +
                "5 BHXH, 4 VRPVC => 5 LTCX";

            var solver = new Day14Solver(input.Split('\n'));
            solver.SolvePart2().Should().Be(460664L);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day14Solver(Day14Solver.ReadRealInput());
            long result = solver.SolvePart2();
            result.Should().Be(1893569L);
        }
    }
}