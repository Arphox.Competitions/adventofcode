﻿using AoC19.Day14;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;

namespace AoC19.Test.Day14
{
    /// <summary>
    ///     Unit tests for <see cref="Day14InputParser"/>
    /// </summary>
    public sealed class Day14InputParserTests
    {
        [Fact]
        public void ParseInputReturnsCorrectOutput_2recipes()
        {
            const string line1 = "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ";
            const string line2 = "7 DCFZ, 3 PSHF => 2 XJWVT";
            IReadOnlyList<Recipe> result = Day14InputParser.ParseInput(new string[] { line1, line2 });

            result.Should().HaveCount(2);

            Recipe recipe1 = result[0];
            recipe1.Amount.Should().Be(9);
            recipe1.Material.Should().Be("QDVJ");
            recipe1.Ingredients.Should().HaveCount(3);

            recipe1.Ingredients[0].Amount.Should().Be(12);
            recipe1.Ingredients[0].Material.Should().Be("HKGWZ");

            recipe1.Ingredients[1].Amount.Should().Be(1);
            recipe1.Ingredients[1].Material.Should().Be("GPVTF");

            recipe1.Ingredients[2].Amount.Should().Be(8);
            recipe1.Ingredients[2].Material.Should().Be("PSHF");

            Recipe recipe2 = result[1];
            recipe2.Amount.Should().Be(2);
            recipe2.Material.Should().Be("XJWVT");
            recipe2.Ingredients.Should().HaveCount(2);

            recipe2.Ingredients[0].Amount.Should().Be(7);
            recipe2.Ingredients[0].Material.Should().Be("DCFZ");

            recipe2.Ingredients[1].Amount.Should().Be(3);
            recipe2.Ingredients[1].Material.Should().Be("PSHF");
        }

        [Fact]
        public void ParseRecipeReturnsCorrectOutput_1ing()
        {
            const string line = "157 ORE => 5 NZVS";
            Recipe result = Day14InputParser.ParseRecipe(line);

            result.Amount.Should().Be(5);
            result.Material.Should().Be("NZVS");

            result.Ingredients.Should().HaveCount(1);

            result.Ingredients[0].Amount.Should().Be(157);
            result.Ingredients[0].Material.Should().Be("ORE");
        }

        [Fact]
        public void ParseRecipeReturnsCorrectOutput_2ing()
        {
            const string line = "7 DCFZ, 17 PSHF => 2 XJWVT";
            Recipe result = Day14InputParser.ParseRecipe(line);

            result.Amount.Should().Be(2);
            result.Material.Should().Be("XJWVT");

            result.Ingredients.Should().HaveCount(2);

            result.Ingredients[0].Amount.Should().Be(7);
            result.Ingredients[0].Material.Should().Be("DCFZ");

            result.Ingredients[1].Amount.Should().Be(17);
            result.Ingredients[1].Material.Should().Be("PSHF");
        }


        [Fact]
        public void ParseRecipeReturnsCorrectOutput_6ing()
        {
            const string line = "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 8 AMNF";
            Recipe result = Day14InputParser.ParseRecipe(line);

            result.Amount.Should().Be(8);
            result.Material.Should().Be("AMNF");

            result.Ingredients.Should().HaveCount(6);

            result.Ingredients[0].Amount.Should().Be(44);
            result.Ingredients[0].Material.Should().Be("XJWVT");

            result.Ingredients[1].Amount.Should().Be(5);
            result.Ingredients[1].Material.Should().Be("KHKGT");

            result.Ingredients[2].Amount.Should().Be(1);
            result.Ingredients[2].Material.Should().Be("QDVJ");

            result.Ingredients[3].Amount.Should().Be(29);
            result.Ingredients[3].Material.Should().Be("NZVS");

            result.Ingredients[4].Amount.Should().Be(9);
            result.Ingredients[4].Material.Should().Be("GPVTF");

            result.Ingredients[5].Amount.Should().Be(48);
            result.Ingredients[5].Material.Should().Be("HKGWZ");
        }
    }
}