﻿using AoC19.Day12;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day12
{
    public sealed class Day12SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day12SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1WebsiteExample1()
        {
            const string input =
                "<x=-1, y=0, z=2>" + "\n" +
                "<x=2, y=-10, z=-7>" + "\n" +
                "<x=4, y=-8, z=8>" + "\n" +
                "<x=3, y=5, z=-1>";

            var solver = new Day12Solver(input.Split('\n'));
            int steps;
            for (steps = 0; steps < 10; steps++)
                solver.P1_DoOneIteration();

            solver.CalculateTotalSystemEnergy().Should().Be(179); // After 10 steps
        }

        [Fact]
        public void Part1WebsiteExample2()
        {
            const string input =
                "<x=-8, y=-10, z=0>" + "\n" +
                "<x=5, y=5, z=10>" + "\n" +
                "<x=2, y=-7, z=3>" + "\n" +
                "<x=9, y=-8, z=-3>";

            var solver = new Day12Solver(input.Split('\n'));
            int steps;
            for (steps = 0; steps < 100; steps++)
                solver.P1_DoOneIteration();

            solver.CalculateTotalSystemEnergy().Should().Be(1940); // After 100 steps
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day12Solver(Day12Solver.ReadRealInput());
            var result = solver.SolvePart1();
            result.Should().Be(8538);
        }

        [Fact]
        public void Part2WebsiteExample1()
        {
            const string input =
                    "<x=-1, y=0, z=2>" + "\n" +
                    "<x=2, y=-10, z=-7>" + "\n" +
                    "<x=4, y=-8, z=8>" + "\n" +
                    "<x=3, y=5, z=-1>";

            var solver = new Day12Solver(input.Split('\n'));
            long result = solver.SolvePart2();
            result.Should().Be(2772);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day12Solver(Day12Solver.ReadRealInput());
            var result = solver.SolvePart2();
            result.Should().Be(506359021038056L);
        }
    }
}