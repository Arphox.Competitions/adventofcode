﻿using AoC19.Day11;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day11
{
    public sealed class Day11SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day11SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day11Solver(Day11Solver.ReadRealInput());
            int result = solver.SolvePart1();
            result.Should().Be(2141);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day11Solver(Day11Solver.ReadRealInput());
            Dictionary<(int x, int y), PaintColor> result = solver.SolvePart2();
            

        }
    }
}