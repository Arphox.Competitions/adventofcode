﻿using AoC19.Day5;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day5
{
    public sealed class Day5SolverTests
    {
        private readonly ITestOutputHelper output;

        public Day5SolverTests(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day5Solver(Day5Solver.ReadRealInput());
            long result = solver.Solve(Day5Solver.AirConditionerUnitId);

            result.Should().Be(9219874);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day5Solver(Day5Solver.ReadRealInput());
            long result = solver.Solve(Day5Solver.ThermalRadiatorControllerId);
            result.Should().Be(5893654);
        }

        [Theory]
        [InlineData(1, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(2, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(3, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(4, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(5, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(6, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(7, 999, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(8, 1000, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(9, 1001, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(10, 1001, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(68, 1001, "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")]
        [InlineData(8, 1, "3,9,8,9,10,9,4,9,99,-1,8")] // PositionMode, Equal 8?
        [InlineData(7, 0, "3,9,8,9,10,9,4,9,99,-1,8")] // PositionMode, Equal 8?        bad
        [InlineData(9, 0, "3,9,8,9,10,9,4,9,99,-1,8")] // PositionMode, Equal 8?        bad
        [InlineData(1, 1, "3,9,7,9,10,9,4,9,99,-1,8")] // PositionMode, Less than 8?
        [InlineData(7, 1, "3,9,7,9,10,9,4,9,99,-1,8")] // PositionMode, Less than 8?
        [InlineData(8, 0, "3,9,7,9,10,9,4,9,99,-1,8")] // PositionMode, Less than 8?    bad
        [InlineData(11, 0, "3,9,7,9,10,9,4,9,99,-1,8")] // PositionMode, Less than 8?
        public void Part2WebsiteExamples(int systemId, int expectedOutput, string input)
        {
            var solver = new Day5Solver(input);
            long result = solver.Solve(systemId);
            result.Should().Be(expectedOutput);
        }
    }
}