﻿using AoC19.Day9;
using FluentAssertions;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day9
{
    public sealed class Day9SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day9SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Theory]
        [InlineData("1091204-1100110011001008100161011006101099", "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")]
        public void Part1WebsiteExamples(string expectedOutput, string input)
        {
            var solver = new Day9Solver(input);
            long[] resultRaw = solver.SolvePart1();
            string result = string.Concat(resultRaw.Select(x => x.ToString()));
            result.Should().Be(expectedOutput);
        }

        [Fact]
        public void Part1WebsiteExample2()
        {
            var solver = new Day9Solver("1102,34915192,34915192,7,4,7,99,0");
            long[] resultRaw = solver.SolvePart1();
            resultRaw[0].ToString().Length.Should().Be(16);
        }

        [Fact]
        public void Part1WebsiteExample3()
        {
            const long largeNumber = 1125899906842624;
            var solver = new Day9Solver($"104,{largeNumber},99");
            long[] resultRaw = solver.SolvePart1();
            resultRaw.Single().Should().Be(largeNumber);
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day9Solver(Day9Solver.ReadRealInput());
            long[] resultRaw = solver.SolvePart1();

            _testOutput.WriteLine("Output:");
            foreach (string item in resultRaw.Select(x => x.ToString()))
                _testOutput.WriteLine(item);

            resultRaw.Should().HaveCount(1).And.BeEquivalentTo(3460311188);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day9Solver(Day9Solver.ReadRealInput());
            long[] resultRaw = solver.SolvePart2();

            _testOutput.WriteLine("Output:");
            foreach (string item in resultRaw.Select(x => x.ToString()))
                _testOutput.WriteLine(item);

            resultRaw.Should().HaveCount(1).And.BeEquivalentTo(42202);
        }
    }
}