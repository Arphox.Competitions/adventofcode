﻿using System.Linq;
using AoC19.Day16;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day16
{
    public sealed class Day16SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day16SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Theory]
        #region [ TestCases ]
        [InlineData(0, new sbyte[0])]
        [InlineData(1, new sbyte[] { 1 })]
        [InlineData(1, new sbyte[] { 1, 0 })]
        [InlineData(1, new sbyte[] { 1, 0, -1 })]
        [InlineData(1, new sbyte[] { 1, 0, -1, 0 })]
        [InlineData(1, new sbyte[] { 1, 0, -1, 0, 1, 0, -1 })]
        [InlineData(1, new sbyte[] { 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0 })]
        [InlineData(1, new sbyte[] { 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1, 0, -1, 0, 1 })]
        [InlineData(2, new sbyte[] { 0 })]
        [InlineData(2, new sbyte[] { 0, 1 })]
        [InlineData(2, new sbyte[] { 0, 1, 1 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0, 0 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0, 0, -1 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0, 0, -1, -1 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0, 0, -1, -1, 0, 0 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0 })]
        [InlineData(2, new sbyte[] { 0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1 })]
        [InlineData(3, new sbyte[] { 0 })]
        [InlineData(3, new sbyte[] { 0, 0 })]
        [InlineData(3, new sbyte[] { 0, 0, 1 })]
        [InlineData(3, new sbyte[] { 0, 0, 1, 1, 1 })]
        [InlineData(3, new sbyte[] { 0, 0, 1, 1, 1, 0, 0, 0 })]
        [InlineData(3, new sbyte[] { 0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, 0, 0, 1 })]
        [InlineData(3, new sbyte[] { 0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, 0 })]
        [InlineData(3, new sbyte[] { 0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1, 0, 0, 0, 1, 1, 1, 0, 0, 0, -1 })]
        [InlineData(4, new sbyte[] { 0, 0, 0, 1, 1, 1 })]
        [InlineData(4, new sbyte[] { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1, -1, -1, -1, 0, 0, 0, 0 })]
        [InlineData(4, new sbyte[] { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1, -1, -1, -1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, -1, -1, -1, -1 })]
        [InlineData(5, new sbyte[] { 0, 0, 0, 0, 1, 1, 1 })]
        [InlineData(5, new sbyte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1 })]
        [InlineData(5, new sbyte[] { 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0 })]
        #endregion
        public void Part1_PatternProviderWorksProperly(int position, sbyte[] expectedValues)
        {
            // Arrange
            var solver = new Day16Solver("");

            // Act
            sbyte[] result = solver.GetPatternEnumerator(position)
                .Take(expectedValues.Length)
                .ToArray();

            // Assert
            result.Should().BeEquivalentTo(expectedValues);
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day16Solver(Day16Solver.ReadRealInput());
            string result = solver.SolvePart1();
            result.Should().Be("30550349");
        }

        private static sbyte[] ToByteDigitsArray(string str)
        {
            return str
                .Select(ch => (sbyte)char.GetNumericValue(ch))
                .ToArray();
        }
    }
}