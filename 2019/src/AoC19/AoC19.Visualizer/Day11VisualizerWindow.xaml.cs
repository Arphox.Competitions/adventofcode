﻿using AoC19.Day11;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace AoC19.Visualizer
{
    public partial class Day11VisualizerWindow : Window
    {
        public Day11VisualizerWindow()
        {
            InitializeComponent();

            var solver = new Day11Solver(Day11Solver.ReadRealInput());
            Dictionary<(int x, int y), PaintColor> result = solver.SolvePart2();

            int minX = result.Keys.Min(k => k.x);
            int minY = result.Keys.Min(k => k.y);
            if (minX < 0 || minY < 0)
            {

            }

            int maxX = result.Keys.Max(k => k.x);
            int maxY = result.Keys.Max(k => k.y);

            const int scale = 16;

            foreach (KeyValuePair<(int x, int y), PaintColor> item in result)
            {
                Brush fillBrush;
                if (item.Value == PaintColor.Black)
                    fillBrush = Brushes.Black;
                else
                    fillBrush = Brushes.Green;

                Rectangle rect = new Rectangle();
                rect.Fill = fillBrush;
                rect.Width = scale;
                rect.Height = scale;

                Canvas.SetTop(rect, item.Key.y * scale);
                Canvas.SetLeft(rect, item.Key.x * scale);

                mycanvas.Children.Add(rect);
            }
        }
    }
}