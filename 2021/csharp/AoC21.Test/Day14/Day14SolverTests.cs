﻿using AoC21.Day14;

namespace AoC21.Test.Day14;

/// <summary>
/// Unit tests for <see cref="Day14Solver"/>
/// </summary>
public sealed class Day14SolverTests
{
    [Theory]
    [InlineData(Example1, "1588")]
    [InlineData(RealInput, "5656")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day14Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "2188189693529")]
    [InlineData(RealInput, "12271437788530")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day14Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";

    private const string RealInput =
@"KHSSCSKKCPFKPPBBOKVF

OS -> N
KO -> O
SK -> B
NV -> N
SH -> V
OB -> V
HH -> F
HP -> H
BP -> O
HS -> K
SN -> B
PS -> C
BS -> K
CF -> H
SO -> C
NO -> H
PP -> H
SS -> P
KV -> B
KN -> V
CC -> S
HK -> H
FN -> C
OO -> K
CH -> H
CP -> V
HB -> N
VC -> S
SP -> F
BO -> F
SF -> H
VO -> B
FF -> P
CN -> O
NP -> H
KK -> N
OP -> S
BH -> F
CB -> V
HC -> P
KH -> V
OV -> V
NK -> S
PN -> F
VV -> N
HO -> S
KS -> C
FP -> F
FH -> F
BB -> C
FB -> V
SB -> K
KP -> B
FS -> C
KC -> P
SC -> C
VF -> F
VN -> B
CK -> C
KF -> H
NS -> C
FV -> K
HV -> B
HF -> K
ON -> S
CV -> N
BV -> F
NB -> N
NN -> F
BF -> N
VB -> V
VS -> K
BK -> V
VP -> P
PB -> F
KB -> C
VK -> O
NF -> F
FO -> F
PH -> N
VH -> B
HN -> B
FK -> K
PO -> H
CO -> B
FC -> V
OK -> F
OF -> V
PF -> F
BC -> B
BN -> O
NC -> K
SV -> H
OH -> B
PC -> O
OC -> C
CS -> P
PV -> V
NH -> C
PK -> H";

    #endregion
}
