﻿using AoC21.Day17;

namespace AoC21.Test.Day17;

/// <summary>
/// Unit tests for <see cref="Day17Solver"/>
/// </summary>
public sealed class Day17SolverTests
{
    [Theory]
    [InlineData(Example1, "45")]
    [InlineData(RealInput, "10296")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day17Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "112")]
    [InlineData(RealInput, "2371")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day17Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 = "target area: x=20..30, y=-10..-5";
    private const string RealInput = "target area: x=96..125, y=-144..-98";

    #endregion
}
