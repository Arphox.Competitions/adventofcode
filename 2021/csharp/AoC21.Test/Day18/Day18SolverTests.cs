﻿using System.Linq;
using AoC21.Day18;

namespace AoC21.Test.Day18;

/// <summary>
/// Unit tests for <see cref="Day18Solver"/>
/// </summary>
public sealed class Day18SolverTests
{
    [Theory]
    [InlineData("1", "2", "[1,2]")]
    [InlineData("1", "[2,3]", "[1,[2,3]]")]
    [InlineData("[1,2]", "3", "[[1,2],3]")]
    [InlineData("[1,2]", "[3,4]", "[[1,2],[3,4]]")]
    [InlineData("[1,2]", "[[3,4],5]", "[[1,2],[[3,4],5]]")]
    [InlineData("[1,1]", "[1,1]")]
    [InlineData("[1,1]", "[2,2]", "[[1,1],[2,2]]")]
    [InlineData("[1,1]", "[2,2]", "[3,3]", "[[[1,1],[2,2]],[3,3]]")]
    [InlineData("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[[[[1,1],[2,2]],[3,3]],[4,4]]")]
    [InlineData("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[[[[3,0],[5,3]],[4,4]],[5,5]]")]
    [InlineData("[1,1]", "[2,2]", "[3,3]", "[4,4]", "[5,5]", "[6,6]", "[[[[5,0],[7,4]],[5,5]],[6,6]]")]
    [InlineData(
        "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]",
        "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
        "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]",
        "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]",
        "[7,[5,[[3,8],[1,4]]]]",
        "[[2,[2,2]],[8,[8,1]]]",
        "[2,9]",
        "[1,[[[9,3],9],[[9,0],[0,7]]]]",
        "[[[5,[7,4]],7],1]",
        "[[[[4,2],2],6],[8,7]]",
        "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")]
    [InlineData(
        "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]",
        "[[[5,[2,8]],4],[5,[[9,9],0]]]",
        "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]",
        "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]",
        "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]",
        "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]",
        "[[[[5,4],[7,7]],8],[[8,3],8]]",
        "[[9,3],[[9,9],[6,[4,9]]]]",
        "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]",
        "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]",
        "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
        )]
    [InlineData("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]", "[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]", "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]")]
    [InlineData("[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]", "[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]", "[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]")]
    [InlineData("[[[[6,7],[6,7]],[[7,7],[0,7]]],[[[8,7],[7,7]],[[8,8],[8,0]]]]", "[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]", "[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]")]
    [InlineData("[[[[7,0],[7,7]],[[7,7],[7,8]]],[[[7,7],[8,8]],[[7,7],[8,7]]]]", "[7,[5,[[3,8],[1,4]]]]", "[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]")]
    [InlineData("[[[[7,7],[7,8]],[[9,5],[8,7]]],[[[6,8],[0,8]],[[9,9],[9,0]]]]", "[[2,[2,2]],[8,[8,1]]]", "[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]")]
    [InlineData("[[[[6,6],[6,6]],[[6,0],[6,7]]],[[[7,7],[8,9]],[8,[8,1]]]]", "[2,9]", "[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]")]
    [InlineData("[[[[6,6],[7,7]],[[0,7],[7,7]]],[[[5,5],[5,6]],9]]", "[1,[[[9,3],9],[[9,0],[0,7]]]]", "[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]")]
    [InlineData("[[[[7,8],[6,7]],[[6,8],[0,8]]],[[[7,7],[5,0]],[[5,5],[5,6]]]]", "[[[5,[7,4]],7],1]", "[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]")]
    [InlineData("[[[[7,7],[7,7]],[[8,7],[8,7]]],[[[7,0],[7,7]],9]]", "[[[[4,2],2],6],[8,7]]", "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")]
    public void Part1_AddTest_WithReduce(params string[] testinput)
    {
        string[] inputs = testinput[0..^1];
        string expectedOutput = testinput[^1];

        SnailfishNumber[] numbers = inputs.Select(Day18Solver.ParseLine).ToArray();
        var result = numbers.Aggregate((a, b) => CompositeSnailfishNumber.Add(a, b));
        result.ToString().Should().Be(expectedOutput);
    }

    [Theory]
    [InlineData("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]")]
    [InlineData("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]")]
    [InlineData("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]")]
    [InlineData("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")] // 2 explosions
    [InlineData("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", "[[3,[2,[8,0]]],[9,[5,[7,0]]]]")]
    [InlineData("[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    public void Part1_ExplodeTest(string input, string expectedOutput)
    {
        SnailfishNumber number = Day18Solver.ParseLine(input);
        SnailfishNumber result = Day18Solver.Reduce(number);
        result.ToString().Should().Be(expectedOutput);
    }

    [Theory]
    [InlineData("10", "[5,5]")]
    [InlineData("11", "[5,6]")]
    [InlineData("12", "[6,6]")]
    public void Part1_SplitTest(string input, string expectedOutput)
    {
        SnailfishNumber number = Day18Solver.ParseLine(input);
        SnailfishNumber result = Day18Solver.Reduce(number);
        result.ToString().Should().Be(expectedOutput);
    }

    [Theory]
    [InlineData("[9,1]", 29)]
    [InlineData("[1,9]", 21)]
    [InlineData("[[9,1],[1,9]]", 129)]
    [InlineData("[[1,2],[[3,4],5]]", 143)]
    [InlineData("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", 1384)]
    [InlineData("[[[[1,1],[2,2]],[3,3]],[4,4]]", 445)]
    [InlineData("[[[[3,0],[5,3]],[4,4]],[5,5]]", 791)]
    [InlineData("[[[[5,0],[7,4]],[5,5]],[6,6]]", 1137)]
    [InlineData("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", 3488)]
    public void Part1_MagnitudeTest(string input, long expectedOutput)
    {
        SnailfishNumber number = Day18Solver.ParseLine(input);
        long result = number.CalculateMagnitude();
        result.Should().Be(expectedOutput);
    }

    [Theory]
    [InlineData("[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    [InlineData("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    [InlineData("[[[[0,7],4],[15,[0,13]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    [InlineData("[[[[0,7],4],[[7,8],[0,13]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    [InlineData("[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    [InlineData("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")]
    [InlineData("[[[[[1,1],[2,2]],[3,3]],[4,4]],[5,5]]", "[[[[3,0],[5,3]],[4,4]],[5,5]]")]
    [InlineData("[[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]],[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]]", "[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]")]
    public void Part1_ReduceTest(string input, string expectedOutput)
    {
        SnailfishNumber number = Day18Solver.ParseLine(input);
        SnailfishNumber result = Day18Solver.Reduce(number);
        result.ToString().Should().Be(expectedOutput);
    }

    [Theory]
    [InlineData(Example1, "4140")]
    [InlineData(RealInput, "3411")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day18Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "3993")]
    [InlineData(RealInput, "4680")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day18Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";

    private const string RealInput =
@"[[[6,[8,3]],[2,0]],[[[9,5],[9,1]],3]]
[[[9,[2,2]],[5,4]],[[[2,2],[9,6]],[7,7]]]
[[[0,[3,2]],1],[[0,[2,8]],[2,[0,4]]]]
[[4,4],[[[7,0],5],[3,1]]]
[[5,4],1]
[[[[7,6],4],9],[[9,1],9]]
[[[1,[7,8]],[[7,7],[1,6]]],[1,[6,[7,1]]]]
[[[[6,8],[5,6]],[[1,1],8]],[[[2,0],[3,1]],[2,[2,6]]]]
[[[6,3],[3,[7,1]]],8]
[[[9,4],[3,[0,6]]],[[2,[3,6]],[[9,8],[1,6]]]]
[9,[0,[[0,7],2]]]
[[[[8,4],7],[[9,2],[0,9]]],[[7,9],[8,[0,9]]]]
[[1,1],[[5,[3,8]],[3,[4,7]]]]
[[[9,[2,9]],[2,[2,9]]],[[[3,5],5],[[3,3],2]]]
[[[[5,4],9],0],[[[5,7],2],[[5,2],9]]]
[[2,[[1,0],[6,2]]],0]
[[[3,7],[7,6]],[[[2,8],5],[3,[9,7]]]]
[[2,[2,[8,8]]],[[[9,9],[1,1]],[[8,6],[0,3]]]]
[[8,1],[3,5]]
[[7,[[7,6],[2,0]]],4]
[[5,4],[[1,3],[5,[2,8]]]]
[7,9]
[[[[6,9],0],[1,[5,0]]],[[[6,4],3],7]]
[[[[3,7],3],[2,6]],[[0,4],[9,9]]]
[[[[1,5],[5,0]],[9,4]],[[[8,3],3],[8,[3,6]]]]
[[[[3,7],5],[[8,5],[1,5]]],[[0,6],[3,4]]]
[[[[4,0],2],[7,[8,4]]],[0,[5,[7,8]]]]
[[[[0,8],[0,4]],[9,3]],[[[5,4],[4,8]],[[1,6],[5,4]]]]
[[0,[0,3]],[[3,[1,5]],[[9,6],[0,6]]]]
[[9,[8,4]],[7,1]]
[[[[1,9],[7,7]],9],[[6,[4,5]],[8,[3,2]]]]
[5,[[2,[9,5]],[3,[4,0]]]]
[[[6,2],[[1,8],5]],6]
[[8,[6,[6,4]]],[0,[[9,8],7]]]
[[[[6,3],[8,0]],[8,[2,7]]],8]
[[[6,[3,6]],[[4,0],[4,7]]],[0,[[4,0],[4,5]]]]
[[[3,[8,1]],1],[2,3]]
[[[6,[7,0]],[[3,5],[3,4]]],7]
[[[[8,0],3],8],[[[1,6],3],[[0,5],2]]]
[[[3,7],[[9,8],8]],[[[8,4],7],[3,[1,7]]]]
[[[0,5],[[5,5],[7,8]]],[9,[5,[2,2]]]]
[[2,9],[[[7,4],4],[[8,0],[6,9]]]]
[[[[7,8],[8,8]],0],9]
[[[4,[0,6]],[[5,9],[0,1]]],[3,[6,7]]]
[[[7,[6,9]],[5,[6,4]]],[[[3,9],6],[[0,1],1]]]
[3,[[[6,9],7],[5,8]]]
[[[3,9],[[3,5],2]],[[[2,5],[4,6]],[8,0]]]
[[[9,7],3],[[[2,7],[0,9]],[3,[0,3]]]]
[[3,[4,0]],[[6,6],[4,5]]]
[[0,0],[[5,9],1]]
[[[6,8],[2,6]],[[[1,1],3],7]]
[[[4,4],[[1,0],[2,4]]],[2,6]]
[[[[6,0],6],[8,[9,9]]],[[4,2],[[1,8],[5,3]]]]
[[[[1,6],[4,3]],[5,5]],[[7,[9,9]],4]]
[[[[6,9],7],[9,3]],[[[9,6],5],0]]
[[3,[[7,2],[8,1]]],[[7,[3,0]],1]]
[0,[0,[1,3]]]
[[[0,5],[[6,1],[4,6]]],[[[0,4],8],[[4,5],9]]]
[[[[7,5],[7,0]],[6,[7,2]]],[7,[3,[4,1]]]]
[[3,3],[0,[6,2]]]
[[[3,8],[[7,3],6]],[[[0,8],3],[[8,9],[2,9]]]]
[[4,[[5,6],[4,0]]],[[7,[7,5]],[5,0]]]
[[[[2,5],[5,4]],9],[[[6,0],[0,0]],[[5,1],8]]]
[[2,[[1,7],7]],[[[4,5],[7,9]],0]]
[[[0,9],[[5,4],3]],3]
[[9,[[1,9],[1,6]]],[[9,[0,3]],[[8,8],[0,7]]]]
[[[[7,2],4],[7,8]],[[[4,1],[3,1]],[2,5]]]
[[[[1,8],3],[2,5]],[[0,[5,8]],[[1,3],[5,2]]]]
[[3,9],[[9,6],[5,[7,1]]]]
[1,[[3,[6,5]],[5,[2,7]]]]
[[[5,8],6],[8,[[9,4],[0,4]]]]
[0,[[5,[6,6]],[[7,4],[4,6]]]]
[[[[6,8],2],[[1,6],[8,2]]],6]
[7,2]
[[3,1],7]
[[[2,[9,5]],0],[[[7,3],4],8]]
[[[[0,0],[4,2]],5],[[8,6],2]]
[[1,[7,8]],[2,[[6,6],[5,7]]]]
[[[3,[6,0]],3],[[7,[4,4]],8]]
[[[9,[8,7]],[[4,2],4]],[[6,1],[[3,3],[2,2]]]]
[[[8,1],[[7,4],[5,9]]],9]
[[[2,[8,6]],[[9,8],2]],[[9,5],[1,[9,8]]]]
[[[[6,1],[3,1]],[[4,5],1]],[[[6,4],[6,2]],2]]
[[[[4,0],[0,1]],[[1,4],6]],7]
[[[[8,9],[0,2]],4],[[[9,8],8],[0,[0,6]]]]
[0,[[[0,9],1],7]]
[[1,[[3,7],3]],[[[2,4],3],0]]
[[[[7,6],3],8],[[5,5],9]]
[[2,[1,3]],[[[6,7],3],[[3,8],7]]]
[[[[0,6],6],6],[[5,[0,9]],[8,[2,4]]]]
[4,[[[3,0],[2,5]],[[7,4],1]]]
[[[[7,9],3],[0,[8,2]]],[[8,[3,4]],[[2,3],[1,6]]]]
[[[3,[6,3]],5],[[3,4],2]]
[[[[1,9],[0,3]],[0,8]],[[[4,2],[4,3]],[[8,9],5]]]
[[[[2,8],[4,9]],[[3,5],6]],[[6,[1,5]],[0,[9,7]]]]
[[6,3],[[[7,7],[1,7]],[[6,5],[0,8]]]]
[[1,[1,[5,8]]],7]
[[0,6],[9,[[3,4],0]]]
[[[[0,2],7],9],9]
[9,6]";

    #endregion
}
