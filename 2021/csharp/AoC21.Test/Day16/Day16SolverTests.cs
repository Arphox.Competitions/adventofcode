﻿using AoC21.Day16;

namespace AoC21.Test.Day16;

/// <summary>
/// Unit tests for <see cref="Day16Solver"/>
/// </summary>
public sealed class Day16SolverTests
{
    [Theory]
    [InlineData("8A004A801A8002F478", "16")] // represents an operator packet (version 4) which contains an operator packet (version 1) which contains an operator packet (version 5) which contains a literal value (version 6); this packet has a version sum of 16.
    [InlineData("620080001611562C8802118E34", "12")] // represents an operator packet (version 3) which contains two sub-packets; each sub-packet is an operator packet that contains two literal values. This packet has a version sum of 12.
    [InlineData("C0015000016115A2E0802F182340", "23")] // has the same structure as the previous example, but the outermost packet uses a different length type ID. This packet has a version sum of 23.
    [InlineData("A0016C880162017C3686B18A3D4780", "31")] // is an operator packet that contains an operator packet that contains an operator packet that contains five literal values; it has a version sum of 31.
    [InlineData("D2FE28", "6")] // 110 100 1 0111 1 1110 0 0101 000
    [InlineData("38006F45291200", "9")] // 00111000000000000110111101000101001010010001001000000000
    [InlineData("EE00D40C823060", "14")] // 11101110000000001101010000001100100000100011000001100000
    [InlineData(RealInput, "960")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day16Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData("C200B40A82", "3")] // finds the sum of 1 and 2, resulting in the value 3.
    [InlineData("04005AC33890", "54")] // finds the product of 6 and 9, resulting in the value 54.
    [InlineData("880086C3E88112", "7")] // finds the minimum of 7, 8, and 9, resulting in the value 7.
    [InlineData("CE00C43D881120", "9")] // finds the maximum of 7, 8, and 9, resulting in the value 9.
    [InlineData("D8005AC2A8F0", "1")] // produces 1, because 5 is less than 15.
    [InlineData("F600BC2D8F", "0")] // produces 0, because 5 is not greater than 15.
    [InlineData("9C005AC2F8F0", "0")] // produces 0, because 5 is not equal to 15.
    [InlineData("9C0141080250320F1802104A08", "1")] // produces 1, because 1 + 3 = 2 * 2.
    [InlineData(RealInput, "12301926782560")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day16Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string RealInput =
@"C20D7900A012FB9DA43BA00B080310CE3643A0004362BC1B856E0144D234F43590698FF31D249F87B8BF1AD402389D29BA6ED6DCDEE59E6515880258E0040A7136712672454401A84CE65023D004E6A35E914BF744E4026BF006AA0008742985717440188AD0CE334D7700A4012D4D3AE002532F2349469100708010E8AD1020A10021B0623144A20042E18C5D88E6009CF42D972B004A633A6398CE9848039893F0650048D231EFE71E09CB4B4D4A00643E200816507A48D244A2659880C3F602E2080ADA700340099D0023AC400C30038C00C50025C00C6015AD004B95002C400A10038C00A30039C0086002B256294E0124FC47A0FC88ACE953802F2936C965D3005AC01792A2A4AC69C8C8CA49625B92B1D980553EE5287B3C9338D13C74402770803D06216C2A100760944D8200008545C8FB1EC80185945D9868913097CAB90010D382CA00E4739EDF7A2935FEB68802525D1794299199E100647253CE53A8017C9CF6B8573AB24008148804BB8100AA760088803F04E244480004323BC5C88F29C96318A2EA00829319856AD328C5394F599E7612789BC1DB000B90A480371993EA0090A4E35D45F24E35D45E8402E9D87FFE0D9C97ED2AF6C0D281F2CAF22F60014CC9F7B71098DFD025A3059200C8F801F094AB74D72FD870DE616A2E9802F800FACACA68B270A7F01F2B8A6FD6035004E054B1310064F28F1C00F9CFC775E87CF52ADC600AE003E32965D98A52969AF48F9E0C0179C8FE25D40149CC46C4F2FB97BF5A62ECE6008D0066A200D4538D911C401A87304E0B4E321005033A77800AB4EC1227609508A5F188691E3047830053401600043E2044E8AE0008443F84F1CE6B3F133005300101924B924899D1C0804B3B61D9AB479387651209AA7F3BC4A77DA6C519B9F2D75100017E1AB803F257895CBE3E2F3FDE014ABC";

    #endregion
}
