﻿using AoC21.Day24;

namespace AoC21.Test.Day24;

/// <summary>
/// Unit tests for <see cref="Day24Solver"/>
/// </summary>
public sealed class Day24SolverTests
{
    [Theory]
    [InlineData(Example1, "xxxx")]
    [InlineData(RealInput, "xxxx")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day24Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "xxxx")]
    [InlineData(RealInput, "xxxx")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day24Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"PLACEHOLDER1";

    private const string RealInput =
@"PLACEHOLDER2";

    #endregion
}
