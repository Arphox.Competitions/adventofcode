﻿using AoC21.Day25;

namespace AoC21.Test.Day25;

/// <summary>
/// Unit tests for <see cref="Day25Solver"/>
/// </summary>
public sealed class Day25SolverTests
{
    [Theory]
    [InlineData(Example1, "xxxx")]
    [InlineData(RealInput, "xxxx")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day25Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "xxxx")]
    [InlineData(RealInput, "xxxx")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day25Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"PLACEHOLDER1";

    private const string RealInput =
@"PLACEHOLDER2";

    #endregion
}
