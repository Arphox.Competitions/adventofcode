﻿namespace AoC21.Day10;

internal sealed class Day10Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        return input.SplitByNewLines()
            .Select(x => P1_CalculateLine(x))
            .Sum()
            .ToString();
    }

    private long P1_CalculateLine(string line)
    {
        Dictionary<char, long> scores = new()
        {
            { ')', 3 },
            { ']', 57 },
            { '}', 1197 },
            { '>', 25137 },
        };

        Stack<char> stack = new();
        for (int i = 0; i < line.Length; i++)
        {
            char ch = line[i];
            try
            {
                if (stack.TryPeek(out char result) && ch == OppositeOfOpening(result))
                {
                    stack.Pop();
                }
                else
                {
                    stack.Push(ch);
                }
            }
            catch (InvalidOperationException) // illegal
            {
                //stack.Reverse().ForEach(ch => Console.Write(ch));
                return scores[stack.Pop()];
            }
        }

        return 0; // incomplete
    }

    private static char OppositeOfOpening(char openingChar)
    {
        return openingChar switch
        {
            '(' => ')',
            '[' => ']',
            '{' => '}',
            '<' => '>',
            _ => throw new InvalidOperationException(),
        };
    }

    internal override string Part2(AoCInput input)
    {
        long[] scores = input.SplitByNewLines()
            .Select(x => P2_CalculateLine(x))
            .Where(x => x != null)
            .Select(sc => sc!.Value)
            .OrderBy(x => x)
            .ToArray();

        long median = scores[scores.Length / 2];
        return median.ToString();
    }

    private long? P2_CalculateLine(string line)
    {
        Dictionary<char, long> scores = new()
        {
            { ')', 1 },
            { ']', 2 },
            { '}', 3 },
            { '>', 4 },
        };

        Stack<char> stack = new();
        for (int i = 0; i < line.Length; i++)
        {
            char ch = line[i];
            try
            {
                if (stack.TryPeek(out char result) && ch == OppositeOfOpening(result))
                {
                    stack.Pop();
                }
                else
                {
                    stack.Push(ch);
                }
            }
            catch (InvalidOperationException) // illegal
            {
                return null;
            }
        }

        // Complete
        long score = 0;
        while (stack.Count > 0)
        {
            score *= 5;
            char currentChar = stack.Pop();
            char oppositeChar = OppositeOfOpening(currentChar);
            score += scores[oppositeChar];
        }

        return score;
    }
}
