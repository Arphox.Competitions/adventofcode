﻿namespace AoC21.Day12;

internal sealed class GraphParser
{
    internal Graph GetGraph() => new(_nodes);

    private readonly Dictionary<string, Node> _nodes = new();

    internal void AddEntry(string line)
    {
        string[] parts = line.Split('-');
        Node leftNode = GetOrAdd(parts[0]);
        Node rightNode = GetOrAdd(parts[1]);

        leftNode.AddNeighbor(rightNode);
        rightNode.AddNeighbor(leftNode);
    }

    private Node GetOrAdd(string label)
    {
        if (_nodes.TryGetValue(label, out Node? existingNode))
            return existingNode!;

        Node newNode = new(label);
        _nodes[label] = newNode;
        return newNode;
    }
}
