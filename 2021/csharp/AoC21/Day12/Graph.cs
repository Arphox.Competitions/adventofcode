﻿namespace AoC21.Day12;

internal sealed class Graph
{
    internal IReadOnlyDictionary<string, Node> NodeDict { get; }
    internal Node Start { get; }
    internal Node End { get; }

    internal Graph(IReadOnlyDictionary<string, Node> nodeDict)
    {
        NodeDict = nodeDict ?? throw new ArgumentNullException(nameof(nodeDict));
        Start = nodeDict["start"];
        End = nodeDict["end"];
    }
}
