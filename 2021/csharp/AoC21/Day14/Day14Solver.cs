﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace AoC21.Day14;

internal sealed class Day14Solver : DaySolver
{
    internal override string Part1(AoCInput inputRaw)
    {
        Day14Input input = ParseInput(inputRaw);
        long result = Calculate(input, 10);
        return result.ToString();
    }

    internal override string Part2(AoCInput inputRaw)
    {
        // Part 2 runs in about ~0.7 ms for the real input as a short measurement.
        Day14Input input = ParseInput(inputRaw);
        long result = Calculate(input, 40);
        return result.ToString();
    }

    private static Day14Input ParseInput(AoCInput inputRaw)
    {
        string[] parts = inputRaw.Value.Split($"{Environment.NewLine + Environment.NewLine}", StringSplitOptions.RemoveEmptyEntries);
        string template = parts[0];
        Rule[] rules = parts[1].Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
            .Select(Rule.Parse)
            .ToArray();

        return new Day14Input(template, rules);
    }

    private static long Calculate(Day14Input input, int totalStepCount)
    {
        Dictionary<Rule, long> ruleCounts = InitRuleCounts(in input);
        Dictionary<Rule, IReadOnlyList<Rule>> ruleGeneratorLookup = InitRuleGenerators(in input);

        // Do steps
        for (int step = 0; step < totalStepCount; step++)
        {
            Dictionary<Rule, long> newRuleCounts = new();

            foreach (KeyValuePair<Rule, long> ruleAndCount in ruleCounts)
            {
                if (ruleAndCount.Value < 1) continue;

                var generatedRules = ruleGeneratorLookup[ruleAndCount.Key];
                newRuleCounts[generatedRules[0]] = newRuleCounts.GetValueOrDefault(generatedRules[0]) + ruleAndCount.Value;
                newRuleCounts[generatedRules[1]] = newRuleCounts.GetValueOrDefault(generatedRules[1]) + ruleAndCount.Value;
            }

            ruleCounts = newRuleCounts;
        }

        // Calculate result
        Dictionary<char, long> charCounts = new();
        foreach (KeyValuePair<Rule, long> ruleAndCount in ruleCounts)
        {
            Rule rule = ruleAndCount.Key;
            long count = ruleAndCount.Value;
            charCounts[rule.Input1] = charCounts.GetValueOrDefault(rule.Input1) + count;
        }
        charCounts[input.Template.Last()] = charCounts.GetValueOrDefault(input.Template.Last()) + 1;

        long mostCommon = charCounts.Max(ch => ch.Value);
        long leastCommon = charCounts.Min(ch => ch.Value);
        long result = mostCommon - leastCommon;
        return result;
    }

    private static Dictionary<Rule, IReadOnlyList<Rule>> InitRuleGenerators(in Day14Input input)
    {
        var dict = new Dictionary<Rule, IReadOnlyList<Rule>>();
        foreach (Rule rule in input.Rules)
        {
            char generated = FindRule(input.Rules, rule.Input1, rule.Input2).Output;

            dict[rule] = new Rule[]
            {
                FindRule(input.Rules, rule.Input1, generated),
                FindRule(input.Rules, generated, rule.Input2),
            };
        }
        return dict;
    }

    private static Dictionary<Rule, long> InitRuleCounts(in Day14Input input)
    {
        Dictionary<Rule, long> ruleCounts = new();

        for (int i = 0; i < input.Template.Length - 1; i++)
        {
            char ch1 = input.Template[i];
            char ch2 = input.Template[i + 1];
            Rule matchingRule = input.Rules.FirstOrDefault(r => r.Input1 == ch1 && r.Input2 == ch2);
            if (matchingRule.Equals(default)) Debugger.Break();

            ruleCounts[matchingRule] = ruleCounts.GetValueOrDefault(matchingRule) + 1L;
        }
        return ruleCounts;
    }

    private static Rule FindRule(IReadOnlyList<Rule> rules, char ch1, char ch2)
    {
        for (int i = 0; i < rules.Count; i++)
            if (rules[i].Input1 == ch1 && rules[i].Input2 == ch2)
                return rules[i];

        throw new Exception("Rule not found");
    }
}

[StructLayout(LayoutKind.Auto)]
internal readonly ref struct Day14Input
{
    internal string Template { get; }
    internal IReadOnlyList<Rule> Rules { get; }
    internal Day14Input(string template, IReadOnlyList<Rule> rules)
    {
        Template = template;
        Rules = rules;
    }
}

[StructLayout(LayoutKind.Auto)]
internal readonly struct Rule : IEquatable<Rule>
{
    internal char Input1 { get; }
    internal char Input2 { get; }
    internal char Output { get; }
    internal Rule(char input1, char input2, char output)
    {
        Input1 = input1;
        Input2 = input2;
        Output = output;
    }
    public override string ToString() => $"{Input1}{Input2} -> {Output}";

    internal static Rule Parse(string line)
    {
        // OS -> N
        string[] parts = line.Split(" -> ");
        char input1 = parts[0][0];
        char input2 = parts[0][1];
        if (parts[0].Length != 2) throw new Exception("we got a problem");
        char output = parts[1].Single();

        return new Rule(input1, input2, output);
    }

    #region [ Equality ]

    public bool Equals(Rule other)
        => other.Input1 == Input1 && other.Input2 == Input2; // a bit dangerous but there cannot be duplicate rules
    public override bool Equals(object? obj) => obj is Rule && Equals((Rule)obj);
    public override int GetHashCode() => HashCode.Combine(Input1, Input2);

    #endregion
}
