﻿namespace AoC21.Day05;

internal readonly struct LineSegment
{
    internal Point Start { get; }
    internal Point End { get; }
    internal LineSegment(Point start, Point end) => (Start, End) = (start, end);
    internal bool IsHorizontal => (Start.X == End.X);
    internal bool IsVertical => (Start.Y == End.Y);
    internal bool IsVerticalOrHorizontal => IsVertical || IsHorizontal;
    public override string ToString() => $"{Start.X},{Start.Y} -> {End.X},{End.Y}";
    internal static LineSegment Parse(string row)
    {
        // e.g. "309,347 -> 309,464"
        string[] parts = row.Split(" -> ");
        if (parts.Length != 2)
            throw new InvalidOperationException("2 parts is expected");
        Point p1 = Point.Parse(parts[0]);
        Point p2 = Point.Parse(parts[1]);
        return new LineSegment(p1, p2);
    }
}
