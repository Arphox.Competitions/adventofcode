﻿namespace AoC21.Day05;

internal readonly struct Point
{
    internal int X { get; }
    internal int Y { get; }
    internal Point(int x, int y) => (X, Y) = (x, y);
    public override string ToString() => $"({X},{Y})";

    internal static Point Parse(string str)
    {
        // e.g. "309,464"
        string[] parts = str.Split(',');
        int x = int.Parse(parts[0]);
        int y = int.Parse(parts[1]);
        return new Point(x, y);
    }
}
