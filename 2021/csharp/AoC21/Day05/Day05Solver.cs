﻿using System.Diagnostics;

namespace AoC21.Day05;

internal sealed class Day05Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        LineSegment[] part1Segments = input.SplitByNewLines()
            .Select(LineSegment.Parse)
            .Where(seg => seg.IsVerticalOrHorizontal)
            .ToArray();

        int[,] map = InitMap(part1Segments);
        FillMap(map, part1Segments);
        // PrintMap(map);
        int atLeast2Count = map.Cast<int>().Count(x => x >= 2);
        return atLeast2Count.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        LineSegment[]? segments = input.SplitByNewLines()
            .Select(LineSegment.Parse)
            .ToArray();

        int[,] map = InitMap(segments);
        FillMap(map, segments);
        PrintMap(map);
        int atLeast2Count = map.Cast<int>().Count(x => x >= 2);
        return atLeast2Count.ToString();
    }

    private static int[,] InitMap(LineSegment[] segments)
    {
        int maxX = segments.SelectMany(s => new[] { s.Start.X, s.End.X }).Max();
        int maxY = segments.SelectMany(s => new[] { s.Start.Y, s.End.Y }).Max();
        if (maxX > 2000 || maxY > 2000)
            Debugger.Break();

        return new int[maxX + 1, maxY + 1];
    }

    private static void FillMap(int[,] map, LineSegment[] segments)
    {
        foreach (LineSegment segment in segments)
            ApplySegment(map, segment);
    }

    private static void ApplySegment(int[,] map, LineSegment segment)
    {
        // Console.WriteLine($"Printing {segment}");
        if (segment.IsVerticalOrHorizontal)
        {
            int x = segment.Start.X;
            int xStep = (segment.End.X - segment.Start.X) >= 0 ? 1 : -1;
            int yStep = (segment.End.Y - segment.Start.Y) >= 0 ? 1 : -1;
            while (x != segment.End.X + xStep)
            {
                int y = segment.Start.Y;
                while (y != segment.End.Y + yStep)
                {
                    map[x, y]++;
                    y += yStep;
                }
                x += xStep;
            }
        }
        else
        {
            int x = segment.Start.X;
            int y = segment.Start.Y;
            int xStep = (segment.End.X - segment.Start.X) >= 0 ? 1 : -1;
            int yStep = (segment.End.Y - segment.Start.Y) >= 0 ? 1 : -1;
            int totalSteps = GetTotalStepCount(segment);
            for (int i = 0; i <= totalSteps; i++)
            {
                map[x, y]++;
                x += xStep;
                y += yStep;
            }
        }
        PrintMap(map);
    }

    private static int GetTotalStepCount(LineSegment segment)
    {
        SanityCheck(segment);
        return Math.Abs(segment.End.X - segment.Start.X);

        static void SanityCheck(LineSegment segment)
        {
            int xDiff = Math.Abs(segment.End.X - segment.Start.X);
            int yDiff = Math.Abs(segment.End.Y - segment.Start.Y);
            if (xDiff != yDiff)
                Debugger.Break();
        }
    }

    private static void PrintMap(int[,] map)
    {
        return;

        for (int y = 0; y < map.GetLength(1); y++)
        {
            for (int x = 0; x < map.GetLength(0); x++)
            {
                Console.Write(map[x, y] == 0 ? "." : map[x, y]);
            }
            Console.WriteLine();
        }
        Console.ReadLine();
        Console.Clear();
    }
}
