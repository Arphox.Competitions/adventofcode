﻿namespace AoC21.Day09;

internal sealed class Day09Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        int[,] map = input.Integer2DArrayWithoutSeparator();

        int riskLevelSum = 0;
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                int currentHeight = map[i,j];
                if (map.GetUDLRNeighbors(i, j).All(nb => currentHeight < nb))
                {
                    riskLevelSum += currentHeight + 1;
                }
            }
        }

        return riskLevelSum.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        Location[,] map = input
            .Integer2DArrayWithoutSeparator()
            .Select(x => new Location(x.Value, x.Index0, x.Index1));

        List<int> basinSizes = new();

        for (int row = 0; row < map.GetLength(0); row++)
        {
            for (int col = 0; col < map.GetLength(1); col++)
            {
                Location loc = map[row, col];
                ProcessLocation(map, loc, basinSizes);
            }
        }

        return basinSizes.OrderByDescending(x => x)
            .Take(3)
            .Product()
            .ToString();
    }

    private static void ProcessLocation(Location[,] map, Location loc, List<int> basinSizes)
    {
        if (loc.Processed || loc.Value == 9)
            return;

        Queue<Location> queue = new();
        queue.Enqueue(loc);
        int size = 0;

        while (queue.Count > 0)
        {
            Location current = queue.Dequeue();
            if (current.Processed) continue;
            size++;
            current.Processed = true;

            queue.EnqueueRange(BasinNeighbors(map, current.RowIndex, current.ColIndex));
        }

        basinSizes.Add(size);
    }

    private static Location[] BasinNeighbors(Location[,] map, int row, int col)
    {
        return map.GetUDLRNeighbors(row, col)
            .Where(x => x.Value < 9)
            .ToArray();
    }
}

internal sealed class Location
{
    public Location(int value, int rowIndex, int colIndex, bool processed = false)
    {
        Value = value;
        RowIndex = rowIndex;
        ColIndex = colIndex;
        Processed = processed;
    }

    public int Value { get; }
    public int RowIndex { get; }
    public int ColIndex { get; }
    public bool Processed { get; set; }

    public override string ToString() => $"{Value}-{Processed} @({RowIndex},{ColIndex})";
}
