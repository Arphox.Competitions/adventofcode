﻿namespace AoC21.Day08;

internal sealed class Day08Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        return input
            .SplitByNewLines()
            .Select(line => line.Split(" | ")[1])
            .Sum(line => line.Split(' ')
                .Sum(segment => segment.Length switch
                {
                    2 or 3 or 4 or 7 => 1,
                    _ => 0
                })
            )
            .ToString();
    }

    internal override string Part2(AoCInput input)
    {
        return input
            .SplitByNewLines()
            .Select(x => ParseLine(x))
            .Select(x => new Deductor(x).GetOutput())
            .Sum()
            .ToString();
    }

    private static LineData ParseLine(string x)
    {
        string[] parts = x.Split(" | ");
        return new LineData
        {
            Segments = parts[0].Split(),
            Output = parts[1].Split(),
        };
    }
}

internal class Deductor
{
    private readonly IReadOnlyList<string> _inputSegments;
    private readonly IReadOnlyList<string> _output;

    private string[] NumberSegments = new string[10]; // 0..9

    internal Deductor(LineData x)
    {
        _inputSegments = x.Segments;
        _output = x.Output;
    }

    internal long GetOutput()
    {
        NumberSegments[1] = _inputSegments.Single(s => s.Length == 2);
        NumberSegments[4] = _inputSegments.Single(s => s.Length == 4);
        NumberSegments[7] = _inputSegments.Single(s => s.Length == 3);
        NumberSegments[8] = _inputSegments.Single(s => s.Length == 7);

        string A_segment = Get_A_segment();
        string B_segment = Get_B_segment();
        string D_segment = Get_D_segment(B_segment);
        string G_segment = Get_G_segment(A_segment, D_segment);
        string F_segment = Get_F_segment(A_segment, B_segment, D_segment, G_segment);
        string C_segment = NumberSegments[1].Select(ch => ch.ToString()).Except(F_segment).Single();
        string E_segment = Get_E_segment(A_segment, B_segment, D_segment, G_segment, F_segment, C_segment);

        NumberSegments[0] = A_segment + B_segment + C_segment + E_segment + F_segment + G_segment;
        NumberSegments[2] = A_segment + C_segment + D_segment + E_segment + G_segment;
        NumberSegments[3] = A_segment + C_segment + D_segment + F_segment + G_segment;
        NumberSegments[5] = A_segment + B_segment + D_segment + F_segment + G_segment;
        NumberSegments[6] = A_segment + B_segment + D_segment + E_segment + F_segment + G_segment;
        NumberSegments[9] = A_segment + B_segment + C_segment + D_segment + F_segment + G_segment;

        string[] outputNumbers = _output.ToArray();
        string resultString = "";
        foreach (string item in outputNumbers)
            resultString += FindValue(item);

        // Console.WriteLine(resultString);
        return int.Parse(resultString);
    }

    string FindValue(string segments)
    {
        if (segments.Length == 2) return "1";
        if (segments.Length == 4) return "4";
        if (segments.Length == 3) return "7";
        if (segments.Length == 7) return "8";

        for (int i = 0; i < NumberSegments.Length; i++)
            if (MyEqual(NumberSegments[i], segments))
                return i.ToString();

        throw new Exception("oh shit");
    }

    static bool MyEqual(string a, string b)
    {
        char[] aOrdered = a.OrderBy(x => x).ToArray();
        char[] bOrdered = b.OrderBy(x => x).ToArray();
        return aOrdered.SequenceEqual(bOrdered);
    }

    private string Get_A_segment()
    {
        return NumberSegments[7].Except(NumberSegments[1]).Single().ToString();
    }

    private string Get_E_segment(string A_segment, string B_segment, string D_segment, string G_segment, string F_segment, string C_segment)
    {
        return NumberSegments[8]
            .Select(ch => ch.ToString())
            .Except(A_segment, B_segment, C_segment, D_segment, F_segment, G_segment)
            .Single();
    }

    private string Get_F_segment(string A_segment, string B_segment, string D_segment, string G_segment)
    {
        string[] fiveLengthNumbers = _inputSegments.Where(s => s.Length == 5).ToArray();
        string[] abdgSegments = new[] { A_segment, B_segment, D_segment, G_segment };

        return fiveLengthNumbers
            .Single(num => num.Select(ch => ch.ToString()).Except(abdgSegments).Count() == 1)
            .Select(ch => ch.ToString())
            .Except(abdgSegments)
            .Single();
    }

    private string Get_G_segment(string A_segment, string D_segment)
    {
        string[] fiveLengthNumbers = _inputSegments.Where(s => s.Length == 5).ToArray();
        return "abcdefg".Select(ch => ch.ToString())
                    .Where(seg => fiveLengthNumbers.Count(s2 => s2.Contains(seg)) == 3)
                    .Except(A_segment, D_segment)
                    .Single();
    }

    private string Get_D_segment(string B_segment)
    {
        return NumberSegments[4].Select(ch => ch.ToString())
            .Except(NumberSegments[1].Select(ch => ch.ToString()))
            .Except(B_segment)
            .Single();
    }

    private string Get_B_segment()
    {
        string[] fiveLengthNumbers = _inputSegments.Where(s => s.Length == 5).ToArray();

        string[] FOUR_BD_segments = NumberSegments[4].Except(NumberSegments[1]).Select(ch => ch.ToString()).ToArray();
        return FOUR_BD_segments.Single(s1 => fiveLengthNumbers.Count(s2 => s2.Contains(s1)) == 1);
    }
}

internal class LineData
{
    public string[] Segments { get; set; }
    public string[] Output { get; set; }
}
