﻿namespace AoC21.Common;

public static class QueueExtensions
{
    public static void EnqueueRange<T>(this Queue<T> source, IEnumerable<T> values)
        => values.ForEach(x => source.Enqueue(x));
}
