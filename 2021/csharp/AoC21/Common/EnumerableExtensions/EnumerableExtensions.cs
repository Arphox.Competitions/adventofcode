﻿namespace AoC21.Common;

public static partial class EnumerableExtensions
{
    public static void ForEach<T>(this IEnumerable<T> value, Action<T> action)
    {
        foreach (var item in value)
            action(item);
    }

    public static IEnumerable<T> Concat<T>(this IEnumerable<T> source, params T[] newValues)
        => source.Concat((IEnumerable<T>)newValues);

    public static IEnumerable<T> Except<T>(this IEnumerable<T> source, params T[] newValues)
        => source.Except((IEnumerable<T>)newValues);
}
