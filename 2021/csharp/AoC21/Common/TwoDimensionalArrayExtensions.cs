﻿using System.Runtime.CompilerServices;
using System.Text;

namespace AoC21.Common;

public static class TwoDimensionalArrayExtensions
{
    /// <summary>
    /// Returns the up-down-left-right neighbors in undefined (but deterministic) order.
    /// </summary>
    /// <remarks>The undefined order is because a direction is ambiguous</remarks>
    public static IEnumerable<T> GetUDLRNeighbors<T>(this T[,] source, int index0, int index1)
    {
        if (source is null)
            throw new ArgumentNullException(nameof(source));

        if (source.IsValidIndex(index0 + 1, index1))
            yield return source[index0 + 1, index1];

        if (source.IsValidIndex(index0 - 1, index1))
            yield return source[index0 - 1, index1];

        if (source.IsValidIndex(index0, index1 + 1))
            yield return source[index0, index1 + 1];

        if (source.IsValidIndex(index0, index1 - 1))
            yield return source[index0, index1 - 1];
    }

    /// <summary>
    /// Returns the indexes up-down-left-right neighbors in undefined (but deterministic) order.
    /// </summary>
    /// <remarks>The undefined order is because a direction is ambiguous</remarks>
    public static IEnumerable<(int index1, int index2)> GetUDLRNeighborIndexes<T>(this T[,] source, int index0, int index1)
    {
        if (source is null)
            throw new ArgumentNullException(nameof(source));

        if (source.IsValidIndex(index0 + 1, index1))
            yield return (index0 + 1, index1);

        if (source.IsValidIndex(index0 - 1, index1))
            yield return (index0 - 1, index1);

        if (source.IsValidIndex(index0, index1 + 1))
            yield return (index0, index1 + 1);

        if (source.IsValidIndex(index0, index1 - 1))
            yield return (index0, index1 - 1);
    }

    /// <summary>
    /// Returns the diagonal neighbors in undefined (but deterministic) order.
    /// Diagonal here means: upleft, upright, downleft, downright neighbors.
    /// </summary>
    /// <remarks>The undefined order is because a direction is ambiguous</remarks>
    public static IEnumerable<T> GetDiagonalNeighbors<T>(this T[,] source, int index0, int index1)
    {
        if (source is null)
            throw new ArgumentNullException(nameof(source));

        if (source.IsValidIndex(index0 + 1, index1 + 1))
            yield return source[index0 + 1, index1 + 1];

        if (source.IsValidIndex(index0 + 1, index1 - 1))
            yield return source[index0 + 1, index1 - 1];

        if (source.IsValidIndex(index0 - 1, index1 - 1))
            yield return source[index0 - 1, index1 - 1];

        if (source.IsValidIndex(index0 - 1, index1 + 1))
            yield return source[index0 - 1, index1 + 1];
    }

    /// <summary>
    /// Returns the indexes of diagonal neighbors in undefined (but deterministic) order.
    /// Diagonal here means: upleft, upright, downleft, downright neighbors.
    /// </summary>
    /// <remarks>The undefined order is because a direction is ambiguous</remarks>
    public static IEnumerable<(int index1, int index2)> GetDiagonalNeighborIndexes<T>(this T[,] source, int index0, int index1)
    {
        if (source is null)
            throw new ArgumentNullException(nameof(source));

        if (source.IsValidIndex(index0 + 1, index1 + 1))
            yield return (index0 + 1, index1 + 1);

        if (source.IsValidIndex(index0 + 1, index1 - 1))
            yield return (index0 + 1, index1 - 1);

        if (source.IsValidIndex(index0 - 1, index1 - 1))
            yield return (index0 - 1, index1 - 1);

        if (source.IsValidIndex(index0 - 1, index1 + 1))
            yield return (index0 - 1, index1 + 1);
    }

    /// <summary>
    /// Returns all neighbors those are closer than the distance of 2 (up, down, left, right, and diagonals too)
    /// in undefined (but deterministic) order.
    /// </summary>
    /// <remarks>The undefined order is because a direction is ambiguous</remarks>
    public static IEnumerable<T> GetNeighborsCloserThanTwo<T>(this T[,] source, int index0, int index1)
    {
        return source.GetUDLRNeighbors(index0, index1)
            .Concat(source.GetDiagonalNeighbors(index0, index1));
    }

    /// <summary>
    /// Returns all neighbors' indexes those are closer than the distance of 2 (up, down, left, right, and diagonals too)
    /// in undefined (but deterministic) order.
    /// </summary>
    /// <remarks>The undefined order is because a direction is ambiguous</remarks>
    public static IEnumerable<(int index1, int index2)> GetIndexesOfNeighborsCloserThanTwo<T>(this T[,] source, int index0, int index1)
    {
        return source.GetUDLRNeighborIndexes(index0, index1)
            .Concat(source.GetDiagonalNeighborIndexes(index0, index1));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsValidIndex<T>(this T[,] source, int index0, int index1)
    {
        return index0 >= 0 && index0 < source.GetLength(0)
            && index1 >= 0 && index1 < source.GetLength(1);
    }

    public static void ForEach<T>(this T[,] source, Action<T> action)
    {
        foreach (T item in source)
            action(item);
    }

    public static TOutput[,] Select<TInput, TOutput>(this TInput[,] source, Func<TInput, TOutput> converter)
    {
        if (source is null) throw new ArgumentNullException(nameof(source));
        if (converter is null) throw new ArgumentNullException(nameof(converter));

        TOutput[,] newArray = new TOutput[source.GetLength(0), source.GetLength(1)];

        for (int i0 = 0; i0 < source.GetLength(0); i0++)
            for (int i1 = 0; i1 < source.GetLength(1); i1++)
                newArray[i0, i1] = converter(source[i0, i1]);

        return newArray;
    }

    public static TOutput[,] Select<TInput, TOutput>(this TInput[,] source, Func<SelectData<TInput>, TOutput> converter)
    {
        if (source is null) throw new ArgumentNullException(nameof(source));
        if (converter is null) throw new ArgumentNullException(nameof(converter));

        TOutput[,] newArray = new TOutput[source.GetLength(0), source.GetLength(1)];

        for (int i0 = 0; i0 < source.GetLength(0); i0++)
            for (int i1 = 0; i1 < source.GetLength(1); i1++)
                newArray[i0, i1] = converter(new SelectData<TInput>(i0, i1, source[i0, i1]));

        return newArray;
    }

    public readonly struct SelectData<T>
    {
        public SelectData(int rowIndex, int colIndex, T value)
        {
            Index0 = rowIndex;
            Index1 = colIndex;
            Value = value;
        }

        public int Index0 { get; }
        public int Index1 { get; }
        public T Value { get; }
    }

    public static string PrintYX<T>(this T[,] source)
    {
        StringBuilder sb = new(source.Length);
        for (int y = 0; y < source.GetLength(0); y++)
        {
            for (int x = 0; x < source.GetLength(1); x++)
            {
                sb.Append(source[y, x]);
            }

            if (y < source.GetLength(0) - 1)
                sb.AppendLine();
        }
        string str = sb.ToString();
        Console.WriteLine(str);
        return str;
    }

    public static void PrintXY<T>(this T[,] source)
    {
        StringBuilder sb = new(source.Length);
        for (int y = 0; y < source.GetLength(1); y++)
        {
            for (int x = 0; x < source.GetLength(0); x++)
            {
                sb.Append(source[x, y]);
            }
            if (y < source.GetLength(0) - 1)
                sb.AppendLine();
        }
        Console.WriteLine(sb.ToString());
    }
}
