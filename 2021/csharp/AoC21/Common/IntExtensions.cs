﻿namespace AoC21.Common;

public static class IntExtensions
{
    public static char ToCharDigit(this int value)
    {
        if (value > 9 || value < 0) throw new ArgumentOutOfRangeException(nameof(value));
        return (char)('0' + value);
    }
}
