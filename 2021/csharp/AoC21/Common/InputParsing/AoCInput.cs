﻿namespace AoC21.Common;

public sealed record AoCInput
{
    public string Value { get; }
    private AoCInput(string value)
    {
        if (string.IsNullOrWhiteSpace(value))
            throw new ArgumentException($"'{nameof(value)}' cannot be null or whitespace.", nameof(value));

        Value = value;
    }

    public static implicit operator string(AoCInput input) => input.Value;
    public static implicit operator AoCInput(string str) => new(str);
}
