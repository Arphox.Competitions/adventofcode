﻿namespace AoC21.Common;

public static class InputParsers
{
    public static string[] SplitByNewLines(this AoCInput str)
        => str.Value.Split(Environment.NewLine);

    #region [ Array of Scalars ]

    public static int[] IntegerArray(this AoCInput input) => input.SplitByNewLines().Select(str => int.Parse(str)).ToArray();

    public static int[] IntegerCsv(this AoCInput input, string separator = ",")
        => input.Value.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

    public static long[] LongArray(this AoCInput input) => input.SplitByNewLines().Select(str => long.Parse(str)).ToArray();

    public static double[] DoubleArray(this AoCInput input) => input.SplitByNewLines().Select(str => double.Parse(str)).ToArray();

    public static decimal[] DecimalArray(this AoCInput input) => input.SplitByNewLines().Select(str => decimal.Parse(str)).ToArray();

    public static string[] StringArray(this AoCInput input) => input.SplitByNewLines();

    public static char[] CharArrayByNewLine(this AoCInput input) => input.SplitByNewLines().Select(str => str.Single()).ToArray();

    #endregion

    #region [ 2-dimensional ]

    public static int[,] Integer2DArrayWithoutSeparator(this AoCInput input)
    {
        // Expects all rows to be the same length
        string[] lines = input.SplitByNewLines();
        int width = lines[0].Length;
        int[,] array = new int[lines.Length, width]; // (row,col)=(Y down,X right)
        for (int row = 0; row < lines.Length; row++)
        {
            string line = lines[row];
            if (line.Length != width)
                throw new Exception("Invalid line length");

            for (int col = 0; col < line.Length; col++)
                array[row, col] = (int)char.GetNumericValue(line[col]);
        }

        return array;
    }

    #endregion
}
