﻿using System.Diagnostics;
using System.Reflection;
Type? typeOverride = null; // typeof(AoC21.Day07.Day07Solver);
DaySolver solver = GetSolver(typeOverride);

Console.WriteLine($"Executing day #{solver.Day} ({solver.GetType().Name})");

Console.WriteLine(solver.Part1Real());
Console.WriteLine(solver.Part2Real());

// RunPerfTest(() => solver.Part1Real(), 60 * 5);
// RunPerfTest(() => solver.Part2Real(), 60 * 5);

Console.ReadLine();

static DaySolver GetSolver(Type? typeOverride)
{
    Type latestSolver = Assembly.GetExecutingAssembly()
    .GetTypes()
    .Where(t => t.IsSubclassOf(typeof(DaySolver)) &&
                !t.IsAbstract &&
                !t.HasCustomAttribute<IgnoreSolverAttribute>())
    .OrderByDescending(t => t.FullName)
    .First();

    Type solverToRun = typeOverride ?? latestSolver;

    return (DaySolver)Activator.CreateInstance(solverToRun)!;
}

static void RunPerfTest(Action action, int SECONDS_TO_RUN = 60 * 5)
{
    Console.WriteLine("Running perf test...");

    // Warmup
    action();

    // Measure baseline
    double baseline = MeasureMicroseconds(action);
    Console.WriteLine($"Baseline = {baseline:f2} us");
    int runCount = (int)(SECONDS_TO_RUN * 1_000_000 / baseline);
    Console.WriteLine($"RunCount = {runCount}");

    // Measure
    double total_us = MeasureMicroseconds(() =>
    {
        for (int i = 0; i < runCount; i++)
            action();
    });

    double result_us = total_us / runCount;
    double result_ms = result_us / 1000;
    Console.WriteLine("Total run time: " + TimeSpan.FromMilliseconds(total_us / 1000));
    Console.WriteLine("Run count: " + runCount);
    Console.WriteLine($"AVG result: {result_us:f4} us = {result_ms:f2} ms");

    // -------------
    static double MeasureMicroseconds(Action act)
    {
        Stopwatch sw = Stopwatch.StartNew();
        act();
        sw.Stop();
        return sw.Elapsed.TotalMilliseconds * 1000;
    }
}
