﻿using System.Text;

namespace AoC21.Day13;

internal sealed class Day13Solver : DaySolver
{
    internal override string Part1(AoCInput inputRaw)
    {
        Day13Input input = ParseInput(inputRaw);
        var instruction = input.Instructions[0];
        int[,] mapXY = ParseMap(input);

        DoFoldInstruction(mapXY, instruction);

        return mapXY.Cast<int>().Count(x => x > 0).ToString();
    }

    private static void DoFoldInstruction(int[,] mapXY, Instruction instruction)
    {
        PrintMap(mapXY);

        if (instruction.Axis == "x")
        {
            for (int x = instruction.Value; x < mapXY.GetLength(0); x++)
            {
                int xDist = x - instruction.Value;
                for (int y = 0; y < mapXY.GetLength(1); y++)
                {
                    int targetX = instruction.Value - xDist;
                    if (targetX >= 0)
                        mapXY[targetX, y] += mapXY[x, y];

                    mapXY[x, y] = 0;
                }
            }
        }
        else if (instruction.Axis == "y")
        {
            for (int x = 0; x < mapXY.GetLength(0); x++)
            {
                for (int y = instruction.Value; y < mapXY.GetLength(1); y++)
                {
                    int yDist = y - instruction.Value;
                    int targetY = instruction.Value - yDist;
                    if (targetY >= 0)
                        mapXY[x, targetY] += mapXY[x, y];
                    mapXY[x, y] = 0;
                }
            }
        }
        else throw new Exception("unknown instruction axis");
        PrintMap(mapXY);
    }

    private static int[,] ParseMap(Day13Input input)
    {
        int maxX = input.Coordinates.Max(c => c.X);
        int maxY = input.Coordinates.Max(c => c.Y);
        int[,] mapXY = new int[maxX + 1, maxY + 1];
        foreach (Coordinate coord in input.Coordinates)
            mapXY[coord.X, coord.Y]++;
        return mapXY;
    }

    internal override string Part2(AoCInput inputRaw)
    {
        Day13Input input = ParseInput(inputRaw);
        int[,] mapXY = ParseMap(input);

        foreach (Instruction instruction in input.Instructions)
        {
            Console.WriteLine(instruction);
            DoFoldInstruction(mapXY, instruction);
        }

        int maxXAfterFold = input.Instructions.Where(x => x.Axis == "x").Min(x => x.Value);
        int maxYAfterFold = input.Instructions.Where(x => x.Axis == "y").Min(x => x.Value);

        using (StreamWriter outputFile = new("part2_output.txt", false))
        {
            for (int y = 0; y < maxYAfterFold; y++)
            {
                StringBuilder sb = new();
                for (int x = 0; x < maxXAfterFold; x++)
                    sb.Append(mapXY[x, y] > 0 ? "#" : ".");

                outputFile.WriteLine(sb.ToString());
            }
        }

        return "PGHZBFJC";
    }

    private static Day13Input ParseInput(AoCInput input)
    {
        string separator = Environment.NewLine + Environment.NewLine;
        string[] parts = input.Value.Split(separator, StringSplitOptions.RemoveEmptyEntries);

        string[] coordinatesRaw = parts[0].Split(Environment.NewLine);
        string[] instructionsRaw = parts[1].Split(Environment.NewLine);

        Coordinate[] coordinates = coordinatesRaw.Select(Coordinate.Parse).ToArray();
        Instruction[] instructions = instructionsRaw.Select(Instruction.Parse).ToArray();

        return new Day13Input(coordinates, instructions);
    }

    private static void PrintMap(int[,] map)
    {
        return;

        for (int y = 0; y < map.GetLength(1); y++)
        {
            for (int x = 0; x < map.GetLength(0); x++)
            {
                Console.Write(map[x, y] == 0 ? "." : "#");
            }
            Console.WriteLine();
        }
        Console.ReadLine();
        Console.Clear();
    }
}

internal readonly struct Day13Input
{
    internal Coordinate[] Coordinates { get; }
    internal Instruction[] Instructions { get; }

    internal Day13Input(Coordinate[] coordinates, Instruction[] instructions)
    {
        Coordinates = coordinates;
        Instructions = instructions;
    }
}

internal readonly struct Instruction
{
    internal string Axis { get; }
    internal int Value { get; }

    internal Instruction(string axis, int value)
    {
        Axis = axis;
        Value = value;
    }

    public override string ToString() => $"fold along {Axis}={Value}";

    internal static Instruction Parse(string line)
    {
        // fold along x=655
        string[] parts = line.Split("fold along ", StringSplitOptions.RemoveEmptyEntries);
        string valueRaw = parts.Single();
        string[] valueParts = valueRaw.Split('=', StringSplitOptions.RemoveEmptyEntries);
        string axis = valueParts[0].ToLower().Single().ToString();
        int value = int.Parse(valueParts[1]);
        return new Instruction(axis, value);
    }
}
