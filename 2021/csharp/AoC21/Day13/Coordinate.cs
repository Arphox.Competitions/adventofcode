﻿namespace AoC21.Day13;

internal readonly struct Coordinate
{
    internal int X { get; }
    internal int Y { get; }
    internal Coordinate(int x, int y) => (X, Y) = (x, y);
    public override string ToString() => $"({X},{Y})";

    internal static Coordinate Parse(string str)
    {
        // e.g. "309,464"
        string[] parts = str.Split(',');
        int x = int.Parse(parts[0]);
        int y = int.Parse(parts[1]);
        return new Coordinate(x, y);
    }
}
