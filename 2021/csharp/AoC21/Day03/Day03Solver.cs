﻿using System.Collections;

namespace AoC21.Day03;

internal sealed class Day03Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        string[] fileRows = input.SplitByNewLines();
        int bitLength = fileRows[0].Length;
        int[] countOfOnesPerBit = new int[bitLength];

        foreach (string row in fileRows)
            for (int i = 0; i < bitLength; i++)
                if (row[i] == '1')
                    countOfOnesPerBit[i]++;

        int reportCount = fileRows.Length;
        BitArray gammaRateBits = new BitArray(bitLength);
        for (int i = 0; i < bitLength; i++)
            gammaRateBits[bitLength - i - 1] = countOfOnesPerBit[i] > reportCount / 2;

        int gammaRate = (int)BitArrayToU64(gammaRateBits);
        int epsilonRate = (int)Math.Pow(2, bitLength) - 1 - gammaRate;
        int powerConsumption = gammaRate * epsilonRate;

        return powerConsumption.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        string[] fileRows = input.SplitByNewLines();

        int oxygenGeneratorRating = CalculateRating(1);
        int co2ScrubberRating = CalculateRating(0);

        int lifeSupportRating = oxygenGeneratorRating * co2ScrubberRating;
        return lifeSupportRating.ToString();

        // --------------------------------------------------------------
        int CalculateRating(int bitCriteria)
        {
            string[]? accumulator = fileRows.ToArray();
            int columnIndex = 0;
            while (accumulator.Length > 1)
            {
                #region [ Select by bit criteria ]
                int selectedBit = (bitCriteria == 1
                    ? GetMostCommon(accumulator, columnIndex)
                    : GetLeastCommon(accumulator, columnIndex))
                    ?? bitCriteria;

                accumulator = accumulator.Where(str => str[columnIndex] == selectedBit.ToCharDigit()).ToArray();
                #endregion
                columnIndex++;
            }

            return Convert.ToInt32(accumulator.Single(), fromBase: 2);
        }
    }

    private static int? GetMostCommon(string[] file, int columnIndex)
    {
        int countOfOnes = file.Count(row => row[columnIndex] == '1');
        int countOfZeros = file.Length - countOfOnes;
        if (countOfOnes > countOfZeros)
            return 1;
        else if (countOfZeros > countOfOnes)
            return 0;
        else
            return null;
    }

    private static int? GetLeastCommon(string[] file, int columnIndex)
        => 1 - GetMostCommon(file, columnIndex);

    public static ulong BitArrayToU64(BitArray ba)
    {
        // https://stackoverflow.com/a/51430897/4215389
        int len = Math.Min(64, ba.Count);
        ulong n = 0;
        for (int i = 0; i < len; i++)
            if (ba.Get(i))
                n |= 1UL << i;
        return n;
    }
}
