﻿namespace AoC21.Day04;

internal readonly struct BingoEngineResult
{
    internal bool HasWinner { get; }
    internal BingoBoard? WinningBoard { get; }
    internal int? WinningScore { get; }

    internal static BingoEngineResult CreateWinning(BingoBoard winningBoard, int winningScore)
    {
        if (winningBoard is null)
            throw new ArgumentNullException(nameof(winningBoard));

        if (winningScore < 0)
            throw new ArgumentOutOfRangeException(
                nameof(winningScore),
                winningScore,
                "Winning score must be positive");

        return new BingoEngineResult(true, winningBoard, winningScore);
    }

    internal static BingoEngineResult CreateNotWinning()
        => new BingoEngineResult(false, null, null);

    private BingoEngineResult(bool hasWinner, BingoBoard? winningBoard, int? winningScore)
    {
        HasWinner = hasWinner;
        WinningBoard = winningBoard;
        WinningScore = winningScore;
    }
}
