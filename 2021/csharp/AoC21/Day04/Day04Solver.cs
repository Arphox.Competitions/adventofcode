﻿namespace AoC21.Day04;

internal sealed class Day04Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        string[] fileRows = input.SplitByNewLines();
        BingoEngine engine = new(fileRows);
        BingoEngineResult result = engine.RunToFirstWinner();
        return result.WinningScore.ToString()!;
    }

    internal override string Part2(AoCInput input)
    {
        string[] fileRows = input.SplitByNewLines();
        BingoEngine engine = new(fileRows);
        BingoEngineResult result = engine.RunToLastWinner();
        return result.WinningScore.ToString()!;
    }
}
