﻿namespace AoC21.Day04;

internal sealed class BingoBoard
{
    private readonly BingoNumber[,] _board;

    internal BingoBoard(string[] fileRows, int fromIndexInclusive, int toIndexExclusive)
    {
        _board = new BingoNumber[5, 5];
        int rowOnBoard = 0;
        for (int rowInInput = fromIndexInclusive; rowInInput < toIndexExclusive; rowInInput++)
        {
            int[]? numbersInRow = fileRows[rowInInput]
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            for (int col = 0; col < 5; col++)
                _board[rowOnBoard, col] = new BingoNumber(numbersInRow[col]);

            rowOnBoard++;
        }
    }

    internal void MarkMatchingNumbers(int number)
    {
        foreach (BingoNumber? item in _board)
            item.MarkIfMatches(number);
    }

    internal int CalculateScore(int lastCalledNumber)
    {
        return _board.Cast<BingoNumber>()
            .Where(x => x.IsNotMarked)
            .Sum(x => x.Value)
            * lastCalledNumber;
    }

    internal bool IsWinning() => IsWinningByRows() || IsWinningByColumns();

    private bool IsWinningByRows()
    {
        for (int row = 0; row < 5; row++)
        {
            bool allMarked = true;

            for (int col = 0; col < 5; col++)
            {
                if (_board[row, col].IsNotMarked)
                {
                    allMarked = false;
                    break;
                }
            }

            if (allMarked)
                return true;
        }

        return false;
    }

    private bool IsWinningByColumns()
    {
        for (int col = 0; col < 5; col++)
        {
            bool allMarked = true;

            for (int row = 0; row < 5; row++)
            {
                if (_board[row, col].IsNotMarked)
                {
                    allMarked = false;
                    break;
                }
            }

            if (allMarked)
                return true;
        }

        return false;
    }
}
