﻿namespace AoC21.Day17;

internal sealed class Day17Input
{
    public int Xfrom { get; }
    public int Xto { get; }
    public int Yfrom { get; }
    public int Yto { get; }

    internal Day17Input(int xfrom, int xto, int yfrom, int yto)
    {
        Xfrom = xfrom;
        Xto = xto;
        Yfrom = yfrom;
        Yto = yto;
    }

    public override string ToString() => $"target area: x={Xfrom}..{Xto}, y={Yfrom}..{Yto}";

    internal static Day17Input Parse(string line)
    {
        // Example input:
        // target area: x=96..125, y=-144..-98

        string[] parts = line.Split(new string[] { "target area: x=", ", y=" }, StringSplitOptions.RemoveEmptyEntries);
        // parts[0]: 96..125
        // parts[1]: -144..-98

        string[] xParts = parts[0].Split("..", StringSplitOptions.RemoveEmptyEntries);
        int xFrom = int.Parse(xParts[0]);
        int xTo = int.Parse(xParts[1]);

        string[] yParts = parts[1].Split("..", StringSplitOptions.RemoveEmptyEntries);
        int yFrom = int.Parse(yParts[0]);
        int yTo = int.Parse(yParts[1]);

        return new Day17Input(xFrom, xTo, yFrom, yTo);
    }
}
