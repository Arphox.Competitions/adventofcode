﻿namespace AoC21.Day15;

internal sealed class Day15Solver : DaySolver
{
    internal override string Part1(AoCInput inputRaw)
    {
        Graph graph = Parse(inputRaw.Integer2DArrayWithoutSeparator());
        DijkstraAlgo dijkstra = new(graph.Nodes);
        long result = dijkstra.ShortestPathWeight(graph.StartNode, graph.EndNode);
        return result.ToString();
    }

    internal override string Part2(AoCInput inputRaw)
    {
        // Runs in around 640 ms
        int[,] mapYX = inputRaw.Integer2DArrayWithoutSeparator();
        mapYX = ExtendForPart2(mapYX);
        Graph graph = Parse(mapYX);
        DijkstraAlgo dijkstra = new(graph.Nodes);
        long result = dijkstra.ShortestPathWeight(graph.StartNode, graph.EndNode);
        return result.ToString();
    }

    private static Graph Parse(int[,] mapYX)
    {
        // Init nodeMap
        Node[,] nodeMap = new Node[mapYX.GetLength(0), mapYX.GetLength(1)];
        for (int row = 0; row < mapYX.GetLength(0); row++)
            for (int col = 0; col < mapYX.GetLength(1); col++)
                nodeMap[row, col] = new Node(mapYX[row, col]);

        Node startNode = nodeMap[0, 0];
        Node endNode = nodeMap[nodeMap.GetLength(0) - 1, nodeMap.GetLength(1) - 1];

        // Set neighbors
        for (int row = 0; row < mapYX.GetLength(0); row++)
        {
            for (int col = 0; col < mapYX.GetLength(1); col++)
            {
                foreach ((int nbRow, int nbCol) in mapYX.GetUDLRNeighborIndexes(row, col))
                {
                    Node nbNode = nodeMap[nbRow, nbCol];
                    nodeMap[row, col].Neighbors.Add(nbNode);
                    nbNode.Neighbors.Add(nodeMap[row, col]);
                }
            }
        }

        return new Graph(startNode, endNode, nodeMap.Cast<Node>().ToArray());
    }

    private static int[,] ExtendForPart2(int[,] mapYX)
    {
        const int SizeMultiplier = 5;
        int[,] newMap = new int[mapYX.GetLength(0) * SizeMultiplier, mapYX.GetLength(1) * SizeMultiplier];
        int originalMapWidth = mapYX.GetLength(1);
        int originalMapHeight = mapYX.GetLength(0);

        for (int row = 0; row < mapYX.GetLength(0); row++)
        {
            for (int col = 0; col < mapYX.GetLength(1); col++)
            {
                for (int i = 0; i < SizeMultiplier; i++)
                {
                    for (int j = 0; j < SizeMultiplier; j++)
                    {
                        int r2 = (i * originalMapHeight) + row;
                        int c2 = (j * originalMapWidth) + col;
                        int increment = i + j;
                        int newValue = mapYX[row, col] + increment;
                        if (newValue >= 10)
                            newValue = (newValue % 10) + 1;
                        newMap[r2, c2] = newValue;
                    }
                }
            }
        }

        return newMap;
    }
}
