﻿namespace AoC21.Day15;

internal sealed class DijkstraAlgo
{
    private readonly Dictionary<Node, long> dist = new();

    private IReadOnlyCollection<Node> nodes;
    private List<Node> Q;
    private readonly Dictionary<Node, Node?> prev = new();

    internal DijkstraAlgo(IReadOnlyCollection<Node> nodes)
    {
        this.nodes = nodes ?? throw new ArgumentNullException(nameof(nodes));
        if (this.nodes.Count < 2)
            throw new ArgumentException("Graph has to contain at least two nodes");
    }

    internal long ShortestPathWeight(Node s, Node t)
    {
        PriorityQueue<Node, long> Q = new();
        foreach (Node v in nodes)
        {
            dist[v] = long.MaxValue;
            prev[v] = null;
            Q.Enqueue(v, long.MaxValue);
        }

        dist[s] = 0;

        while (Q.Count > 0)
        {
            Node u = Q.Dequeue();
            u.IsProcessed = true;

            if (u == t)
                return dist[t];

            foreach (Node v in u.Neighbors.Where(n => !n.IsProcessed))
            {
                long alt = dist[u] + v.Weight;
                if (alt < dist[v])
                {
                    dist[v] = alt;
                    prev[v] = u;
                    Q.Enqueue(v, alt); // I still don't understand how this works as "Q.decrease_priority(v, alt)"
                }
            }
        }

        return dist[t];
    }
}
